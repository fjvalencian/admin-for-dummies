<?php

namespace Mediadiv\homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Slider;
use Mediadiv\AdminBundle\Entity\Categorias;
use Mediadiv\AdminBundle\Entity\SubCategorias;
use Mediadiv\AdminBundle\Entity\Productos;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Doctrine\Common\Util\Debug;

class DefaultController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();


        $Tiposervicios = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array('titulo' => 'NUESTROS SERVICIOS'));
        
        $seccion = $em->getRepository('MediadivAdminBundle:Seccion')->findAll();
        $seccionMenu = "home"; 
        

        $analytics = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->findOneBy(array('estado' => '1'));
        $redesSociales = $em->getRepository('MediadivAdminBundle:RedesSociales')->findBy(array(), array('id' => 'DESC'));
        $servicio = $em->getRepository('MediadivAdminBundle:Seccion')->findBy(array('tiposeccion' => $Tiposervicios), array('id' => 'DESC'));
        $title = $em->getRepository('MediadivAdminBundle:Titles')->findOneBy(array('estado' => '1'));

        $posTop = $em->getRepository('MediadivAdminBundle:Posicion')->findOneBy(array('nombre' => 'Top'));
        $posBot = $em->getRepository('MediadivAdminBundle:Posicion')->findOneBy(array('nombre' => 'Bottom'));
        $seccionHome = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array('titulo' => 'home'));
            
        $sliderTop = $em->getRepository('MediadivAdminBundle:Slider')->findBy(array('posicion' => $posTop , "tiposeccion" => $seccionHome ));
        $sliderBottom = $em->getRepository('MediadivAdminBundle:Slider')->findBy(array('tiposeccion' => $seccionHome, 'posicion' => $posBot));


        $tipoSeccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findBy(array(), array('orden' => 'asc'));
        $noticias = $em->getRepository('MediadivAdminBundle:Noticias')->findBy(array(), array('id' => 'DESC'));
        $seccionHome = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array('titulo' => 'HOME'));
        #$tipocarrusel = $em->getRepository('MediadivAdminBundle:TipoAccordion')->findBy(array('id' => 'DESC'));
        #$tipoestadisticas = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->findBy(array('tiposeccion' => 'HOME'));
        
        $tipoAccordion = $em->getRepository('MediadivAdminBundle:TipoAccordion')->findBy(array('tiposeccion' => $seccionHome));

        $TipoEstadisticasTop = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')
                                ->findBy(array('tiposeccion' => $seccionHome));
        $TipoEstadisticasBottom = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')
                                ->findBy(array('posicion' => $posBot, 'tiposeccion' => $seccionHome ));

    
       

        $contacto = $em->getRepository('MediadivAdminBundle:Contacto')->findOneBy(array('id' => '1'));
        
                
        $analiticsScript = $analytics->getScript();
        $titulo = $title->getTitle();
        $meta = "prueba";
 
        $nombreseccion = "index";



        return $this->render('MediadivhomeBundle:Default:index.html.twig', array(
                    'seccion' => $seccion,
                    'tipoSeccion' => $tipoSeccion,
                    'seccion' => $seccion,
                    'servicios' => $servicio,
                    'TipoEstadisticasTop' => $TipoEstadisticasTop,
                    'TipoEstadisticasBottom' => $TipoEstadisticasBottom,
                    'tipoAccordion' => $tipoAccordion,
                    'noticias' => $noticias,
                    'redesSociales' => $redesSociales,
                    'tipoSeccion' => $tipoSeccion,
                    'titulo' => $titulo,
                    'nombreseccion' => $nombreseccion,
                    'meta' => $meta,
                    'analytics' => $analiticsScript,
                    'sliderTop' => $sliderTop,
                    "sliderBottom" => $sliderBottom ,
                    'contacto' => $contacto,
                    "seccionMenu" =>  $seccionMenu
         ));
    }

    public function tipoSeccionAction($id, $titulo) {


        $em = $this->getDoctrine()->getManager();
        
        $seccion = $em->getRepository('MediadivAdminBundle:Seccion')->findAll();
        
        $seccionActual = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                        ->findOneBy(array('id' => $id));

       
        $nombreseccion = $seccionActual->getTitulo();

                $seccionMenu = $seccionActual->getTitulo(); 

        $tiposSeccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                        ->findBy(array(), array('orden' => 'asc'));
        
        $redesSociales = $em->getRepository('MediadivAdminBundle:RedesSociales')
                        ->findBy(array(), array('id' => 'DESC'));
        
        $title = $em->getRepository('MediadivAdminBundle:Titles')
                 ->findOneBy(array('estado' => '1'));
        
        $analytics = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')
                    ->findOneBy(array('estado' => '1'));


        $analiticsScript = $analytics->getScript();
        $titulo = $title->getTitle();
        $meta = "prueba";
        $plantilla = $seccionActual->getTipoplantilla();





        $posTop = $em->getRepository('MediadivAdminBundle:Posicion')->findOneBy(array('nombre' => 'Top'));
        $posBottom = $em->getRepository('MediadivAdminBundle:Posicion')->findOneBy(array('nombre' => 'Bottom'));
        
        $sliderTop = $em->getRepository('MediadivAdminBundle:Slider')->findBy(array('tiposeccion' => $seccionActual, 'posicion' => $posTop));
        $sliderBottom = $em->getRepository('MediadivAdminBundle:Slider')->findBy(array('tiposeccion' => $seccionActual, 'posicion' => $posBottom));


        $TipoAccordionBottom = $em->getRepository('MediadivAdminBundle:TipoAccordion')
                                ->findBy(array('posicion' => $posBottom, 'tiposeccion' => $seccionActual->getId()));

        $TipoAccordionTop = $em->getRepository('MediadivAdminBundle:TipoAccordion')
                            ->findBy(array('posicion' => $posTop, 'tiposeccion' => $seccionActual));

        $TipoEstadisticasTop = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')
                                ->findBy(array('posicion' => $posTop, 'tiposeccion' => $seccionActual));
        $TipoEstadisticasBottom = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')
                                ->findBy(array('posicion' => $posBottom, 'tiposeccion' => $seccionActual->getId()));

        

        $TipoAsociadosTop = $em->getRepository('MediadivAdminBundle:TipoAsociados')
                            ->findBy(array('posicion' => $posTop,'tiposeccion' => $seccionActual));
        $TipoAsociadosBottom = $em->getRepository('MediadivAdminBundle:TipoAsociados')
                            ->findBy(array('posicion' => $posBottom, 'tiposeccion' => $seccionActual));



        $TipoEmpresasTop = $em->getRepository('MediadivAdminBundle:TipoEmpresa')
                            ->findBy(array('posicion' => $posTop,'tiposeccion' => $seccionActual));
        $TipoEmpresasBottom = $em->getRepository('MediadivAdminBundle:TipoEmpresa')
                            ->findBy(array('posicion' => $posBottom,'tiposeccion' => $seccionActual));



        $TipoClienteTop = $em->getRepository('MediadivAdminBundle:TipoCliente')
                        ->findBy(array('posicion' => $posTop,'tiposeccion' => $seccionActual));
        $TipoClienteBottom = $em->getRepository('MediadivAdminBundle:TipoCliente')
                         ->findBy(array('posicion' => $posBottom,'tiposeccion' => $seccionActual));
        
        $TipoAccordionTop = $em->getRepository('MediadivAdminBundle:TipoAccordion')
                            ->findBy(array('posicion' => $posTop,'tiposeccion' => $seccionActual));
        $TipoAccordionBottom = $em->getRepository('MediadivAdminBundle:TipoAccordion')
                            ->findBy(array('posicion' => $posBottom ,'tiposeccion' => $seccionActual));

        $contacto = $em->getRepository('MediadivAdminBundle:Contacto')->findOneBy(array('id' => '1'));
        


        if ($plantilla == 'plantilla1') {

            return $this->render('MediadivhomeBundle:Default:seccionTipo1.html.twig', array(
                        'seccion' => $seccion,
                        'tipoSeccion' => $tiposSeccion,
                        'seccionActual' => $seccionActual,
                        'redesSociales' => $redesSociales,
                        'titulo' => $titulo,
                        'nombreseccion' => $nombreseccion,
                        'meta' => $meta,
                        'analytics' => $analiticsScript,
                        'TipoAsociadosTop' => $TipoAsociadosTop,
                        'TipoAsociadosBottom' => $TipoAsociadosBottom,
                        'TipoEmpresasTop' => $TipoEmpresasTop,
                        'TipoEmpresasBottom' => $TipoEmpresasBottom,
                        'TipoClienteTop' => $TipoClienteTop,
                        'TipoClienteBottom' => $TipoClienteBottom,
                        'sliderTop' => $sliderTop,
                        'sliderBottom' => $sliderBottom,
                        'TipoAccordionBottom' => $TipoAccordionBottom,
                        'TipoAccordionTop' => $TipoAccordionTop,
                        'TipoEstadisticasTop' => $TipoEstadisticasTop,
                        'TipoEstadisticasBottom' => $TipoEstadisticasBottom,
                        'contacto' => $contacto,
                        "seccionMenu" => $seccionMenu 
            ));
        }


        if ($plantilla == 'plantilla2') {


            return $this->render('MediadivhomeBundle:Default:seccionTipo2.html.twig', array(
                        'seccion' => $seccion,
                        'tipoSeccion' => $tiposSeccion,
                        'seccionActual' => $seccionActual,
                        'redesSociales' => $redesSociales,
                        'titulo' => $titulo,
                        'nombreseccion' => $nombreseccion,
                        'meta' => $meta,
                        'analytics' => $analiticsScript,
                        'TipoAsociadosTop' => $TipoAsociadosTop,
                        'TipoAsociadosBottom' => $TipoAsociadosBottom,
                        'TipoEmpresasTop' => $TipoEmpresasTop,
                        'TipoEmpresasBottom' => $TipoEmpresasBottom,
                        'TipoClienteTop' => $TipoClienteTop,
                        'TipoClienteBottom' => $TipoClienteBottom,
                        'sliderTop' => $sliderTop,
                        'sliderBottom' => $sliderBottom,
                        'TipoAccordionBottom' => $TipoAccordionBottom,
                        'TipoAccordionTop' => $TipoAccordionTop,
                        'TipoEstadisticasTop' => $TipoEstadisticasTop,
                        'TipoEstadisticasBottom' => $TipoEstadisticasBottom,
                        'contacto' => $contacto,
                        "seccionMenu" => $seccionMenu 
            ));
        }
    }

    public function loginAction() {

        $request = $this->getRequest();
        $session = $request->getSession();




        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }



        return $this->render(
                        'MediadivAdminBundle:Default:login.html.twig', array(
                    // last username entered by the user


                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                        )
        );
    }

    public function contactoAction() {
        $em = $this->getDoctrine()->getManager();

        $seccion = $em->getRepository('MediadivAdminBundle:Seccion')->findAll();
        $redesSociales = $em->getRepository('MediadivAdminBundle:RedesSociales')->findBy(array(), array('id' => 'DESC'));
        $tiposSeccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findBy(array(), array('orden' => 'asc'));
        $contacto = $em->getRepository('MediadivAdminBundle:Contacto')->findOneBy(array('id' => '1'));
        $title = $em->getRepository('MediadivAdminBundle:Titles')->findOneBy(array('estado' => '1'));
        $analytics = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->findOneBy(array('estado' => '1'));
        $analiticsScript = $analytics->getScript();
        $titulo = $title->getTitle();
        $meta = "prueba";
        $seccionMenu = "contacto";
        $nombreseccion = "contacto";



        return $this->render('MediadivhomeBundle:Default:contacto.html.twig', array(
                    'seccion' => $seccion,
                    'tipoSeccion' => $tiposSeccion,
                    'contacto' => $contacto,
                    'redesSociales' => $redesSociales,
                    'titulo' => $titulo,
                    'nombreseccion' => $nombreseccion,
                    'meta' => $meta,
                    'analytics' => $analiticsScript,
                    "seccionMenu" => $seccionMenu
        ));
    }

    public function enviaCorreoAction(Request $request) {

        $nombre = $request->request->get('name');
        $correo = $request->request->get('email');
        $city = $request->request->get('city');
        $asunto = $request->request->get('subject');
        $mensaje = $request->request->get('message');




        $message = \Swift_Message::newInstance()
                ->setSubject('Request   Contacto')
                ->setFrom('giorgosbarkos@gmail.com')
                ->setTo('giorgosbarkos@gmail.com')
                ->setBody(
                $this->renderView(
                        'MediadivhomeBundle:Default:enviaCorreo.html.twig', array(
                    'nombre' => $nombre,
                    'correo' => $correo,
                    'asunto' => $asunto,
                    'city' => $city,
                    'mensaje' => $mensaje
                        )
                )
        );
        $this->get('mailer')->send($message);


        $this->get('session')->getFlashBag()->add('contacto', 'Email enviado Correctamente');

        return $this->redirect($this->generateUrl('contacto'));
    }

}
