<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoNoticias;
use Mediadiv\AdminBundle\Form\TipoNoticiasType;

/**
* TipoNoticias controller.
*
*/
class TipoNoticiasController extends Controller
{

/**
 * Lists all TipoNoticias entities.
 *
 */
public function indexAction()
{
    $em = $this->getDoctrine()->getManager();

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    $entities = $em->getRepository('MediadivAdminBundle:TipoNoticias')->findAll();

    return $this->render('MediadivAdminBundle:TipoNoticias:index.html.twig', array(
        'entities' => $entities,
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
    ));
}
/**
 * Creates a new TipoNoticias entity.
 *
 */
public function createAction(Request $request)
{
        $entity = new TipoNoticias();
       
        $titulo = $request->request->get('mediadiv_adminbundle_tiponoticias_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tiponoticias_descripcion');

        $em = $this->getDoctrine()->getManager();

        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $em->persist($entity);
        $em->flush();

        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
}

/**
 * Creates a form to create a TipoNoticias entity.
 *
 * @param TipoNoticias $entity The entity
 *
 * @return \Symfony\Component\Form\Form The form
 */
private function createCreateForm(TipoNoticias $entity)
{
    $form = $this->createForm(new TipoNoticiasType(), $entity, array(
        'action' => $this->generateUrl('admin_tiponoticias_create'),
        'method' => 'POST',
    ));

    #$form->add('submit', 'submit', array('label' => 'Create'));

    return $form;
}

/**
 * Displays a form to create a new TipoNoticias entity.
 *
 */
public function newAction()
{
    $entity = new TipoNoticias();
    $form   = $this->createCreateForm($entity);

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    return $this->render('MediadivAdminBundle:TipoNoticias:new.html.twig', array(
        'entity' => $entity,
        'form'   => $form->createView(),
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
    ));
}

/**
 * Finds and displays a TipoNoticias entity.
 *
 */
public function showAction($id)
{
    $em = $this->getDoctrine()->getManager();

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    $entity = $em->getRepository('MediadivAdminBundle:TipoNoticias')->find($id);

    if (!$entity) {
        throw $this->createNotFoundException('Unable to find TipoNoticias entity.');
    }

    $deleteForm = $this->createDeleteForm($id);

    return $this->render('MediadivAdminBundle:TipoNoticias:show.html.twig', array(
        'entity'      => $entity,
        'delete_form' => $deleteForm->createView(),
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
    ));
}

/**
 * Displays a form to edit an existing TipoNoticias entity.
 *
 */
public function editAction($id)
{
    $em = $this->getDoctrine()->getManager();

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    $entity = $em->getRepository('MediadivAdminBundle:TipoNoticias')->find($id);

    if (!$entity) {
        throw $this->createNotFoundException('Unable to find TipoNoticias entity.');
    }

    $editForm = $this->createEditForm($entity);
    $deleteForm = $this->createDeleteForm($id);

    return $this->render('MediadivAdminBundle:TipoNoticias:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
    ));
}

/**
* Creates a form to edit a TipoNoticias entity.
*
* @param TipoNoticias $entity The entity
*
* @return \Symfony\Component\Form\Form The form
*/
private function createEditForm(TipoNoticias $entity)
{
    $form = $this->createForm(new TipoNoticiasType(), $entity, array(
        'action' => $this->generateUrl('admin_tiponoticias_update', array('id' => $entity->getId())),
        'method' => 'PUT',
    ));

    $form->add('submit', 'submit', array('label' => 'Editar',
        'attr' => array('class' => 'btn btn-success')));

    return $form;
}
/**
 * Edits an existing TipoNoticias entity.
 *
 */
public function updateAction(Request $request, $id)
{
    $em = $this->getDoctrine()->getManager();

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    $entity = $em->getRepository('MediadivAdminBundle:TipoNoticias')->find($id);

    if (!$entity) {
        throw $this->createNotFoundException('Unable to find TipoNoticias entity.');
    }

    $deleteForm = $this->createDeleteForm($id);
    $editForm = $this->createEditForm($entity);
    $editForm->handleRequest($request);

    if ($editForm->isValid()) {
        $em->flush();

        return $this->redirect($this->generateUrl('admin_tiponoticias_edit', 
            array(
                'id' => $id,
                'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid)));
    }

    return $this->render('MediadivAdminBundle:TipoNoticias:edit.html.twig', array(
        'entity'      => $entity,
        'edit_form'   => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
    ));
}
/**
 * Deletes a TipoNoticias entity.
 *
 */
public function deleteAction(Request $request, $id)
{
    $form = $this->createDeleteForm($id);
    $form->handleRequest($request);

    $user = $this->get('security.context')->getToken()->getUser();
    $nombrerol = $user->getRoles();
    $nombreuser = $user->getUsername();
    $userid = $user->getId();

    if ($form->isValid()) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:TipoNoticias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoNoticias entity.');
        }

        $em->remove($entity);
        $em->flush();
    }

    return $this->redirect($this->generateUrl('admin_tiponoticias', array(
        'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        )));
}

/**
 * Creates a form to delete a TipoNoticias entity by id.
 *
 * @param mixed $id The entity id
 *
 * @return \Symfony\Component\Form\Form The form
 */
private function createDeleteForm($id)
{
    return $this->createFormBuilder()
        ->setAction($this->generateUrl('admin_tiponoticias_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array('label' => 'Delete'))
        ->getForm()
    ;
}
}
