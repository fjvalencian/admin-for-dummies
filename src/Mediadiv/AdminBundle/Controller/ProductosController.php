<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Mediadiv\AdminBundle\Entity\Productos;
use Mediadiv\AdminBundle\Entity\FotosProductos;
use Mediadiv\AdminBundle\Form\ProductosType;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Productos controller.
 *
 */
class ProductosController extends Controller
{

    /**
     * Lists all Productos entities.
     *
     */
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
            $entities = $em->getRepository('MediadivAdminBundle:Productos')->findAll();

            return $this->render('MediadivAdminBundle:Productos:index.html.twig', array(
                'entities' => $entities,
            ));
                
       
    }
    /**
     * Creates a new Productos entity.
     *
     */
    public function createAction(Request $request)
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
       
            $entity = new Productos();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $entity->setFechaIngreso(new \DateTime());
                $em->persist($entity);
                $em->flush();

                $this->getSession('session')->getFlashBag()->add(
                        'producto',
                        'Tu Producto se ha guardado.'
                        );

                return $this->redirect($this->generateUrl('admin_productos', array('id' => $entity->getId())));
            }

            return $this->render('MediadivAdminBundle:Productos:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));

        
    }

    /**
    * Creates a form to create a Productos entity.
    *
    * @param Productos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Productos $entity)
    {
        $form = $this->createForm(new ProductosType(), $entity, array(
            'action' => $this->generateUrl('admin_productos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Productos entity.
     *
     */
    public function newAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
     
            $entity = new Productos();
            $form   = $this->createCreateForm($entity);

            return $this->render('MediadivAdminBundle:Productos:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
                
          
    }

    /**
     * Finds and displays a Productos entity.
     *
     */
    public function showAction($id)
    {
        
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
       
            $entity = $em->getRepository('MediadivAdminBundle:Productos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productos entity.');
            }

            $deleteForm = $this->createDeleteForm($id);

            return $this->render('MediadivAdminBundle:Productos:show.html.twig', array(
                'entity'      => $entity,
                'delete_form' => $deleteForm->createView(),        ));
            
       
    }

    /**
     * Displays a form to edit an existing Productos entity.
     *
     */
    public function editAction($id)
    {

        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
      
            $entity = $em->getRepository('MediadivAdminBundle:Productos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productos entity.');
            }

            $editForm = $this->createEditForm($entity);
            $deleteForm = $this->createDeleteForm($id);

            return $this->render('MediadivAdminBundle:Productos:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
     
    }

    /**
    * Creates a form to edit a Productos entity.
    *
    * @param Productos $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Productos $entity)
    {
        $form = $this->createForm(new ProductosType(), $entity, array(
            'action' => $this->generateUrl('admin_productos', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));



        $idProducto = $entity->getId();
        $session = $this->getRequest()->getSession();

        $session->set('idProducto', $idProducto);


        $form->add('submit', 'submit', array('label' => 'Editar'));

        return $form;
    }
    /**
     * Edits an existing Productos entity.
     *
     */
public function updateAction(Request $request, $id)
    {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario'  => $nusuario , 'password' => $contrasena));
        
        
        if ($admin) {
           $em = $this->getDoctrine()->getManager();
           $entity = $em->getRepository('MediadivAdminBundle:Productos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productos entity.');
            }

            $deleteForm = $this->createDeleteForm($id);
            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);


            if ($editForm->isValid()) {
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'productos',

                        'Tu producto se ha editado correctamente.'

                        );

                return $this->redirect($this->generateUrl('admin_productos'));
            }

            return $this->render('MediadivAdminBundle:Productos:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
            
        }else{
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista'));  
        }
    }
    /**
     * Deletes a Productos entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:Productos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productos entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_productos'));
    }
    
    public function deleteProductoAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Productos')->find($id);
        
        $em->remove($entity);
        $em->flush();
        
        return $this->redirect($this->generateUrl('admin_productos'));
        
    }

    /**
     * Creates a form to delete a Productos entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_productos_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
    
    
    
    public function vistaEditarProductoAction($id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario'  => $nusuario , 'password' => $contrasena));
        
        
        if ($admin) {
        $em = $this->getDoctrine()->getManager();
        
        $producto = $em->getRepository('MediadivAdminBundle:Productos')->find($id);
        $categoria = $em->getRepository('MediadivAdminBundle:Categorias')->findAll();
        return $this->render('MediadivAdminBundle:Productos:vistaEditarProducto.html.twig', array('producto' => $producto, 'categoria' => $categoria));
        }else{
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista')); 
        }
    }
    
    
    public function guardarEditarProductoAction(Request $request) {
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getManager();
        
        $id = $request->request->get('id');


        $nombre = $request->request->get('nombreProducto');
        $codigo = $request->request->get('codigoProducto');
        $descripcion = $request->request->get('descripcionProducto');
        $descripcionCorta = $request->request->get('descripcionCortaProducto');
        $caracteristicaUno = $request->request->get('caracteristicaUnoProducto');
        $stock = $request->request->get('sotckProducto');
        $estado = $request->request->get('estadoProducto');
        $subcategoriaid = $request->request->get('subcategoriaProducto');

        
        $subcategoria = $em->getRepository('MediadivAdminBundle:SubCategorias')->findOneBy(array('id'=>$subcategoriaid));
        $producto = $em->getRepository('MediadivAdminBundle:Productos')->findOneBy(array('id'=> $id ));

     
        
        


        $producto->setNombre($nombre);
        $producto->setCodigo($codigo);
        $producto->setDescripcion($descripcion);
        $producto->setDescripcioncorta($descripcionCorta);
        $producto->setCaracteristicasuno($caracteristicaUno);
        $producto->setStock($stock);
        $producto->setEstado($estado);
        $producto->setSubcategorias($subcategoria);
        $producto->setFechaIngreso(new \DateTime);
        
        
        $em->persist($producto);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
                'productos',
                'Se ha editado '. $producto->getNombre() .' correctamente.'
                );
        
        $idProducto = $producto->getId();
        
        $session->set('idProducto', $idProducto);
        
         $producto = $em->getRepository('MediadivAdminBundle:Productos')->find($id);
        $categoria = $em->getRepository('MediadivAdminBundle:Categorias')->findAll();
        return $this->render('MediadivAdminBundle:Productos:vistaEditarProducto.html.twig', array('producto' => $producto, 'categoria' => $categoria));
        
        
        //return $this->redirect($this->generateUrl('admin_productos_vistaUploadProducto'));
    }
    
    
    public function vistaGuardarProductoAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario'  => $nusuario , 'password' => $contrasena));
        
        if ($admin) {
            
            $categoria = $em->getRepository('MediadivAdminBundle:Categorias')->findAll();
            return $this->render('MediadivAdminBundle:Productos:vistaGuardarProducto.html.twig', array('categoria' => $categoria));
                
        }else{
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista'));  
        }
    }
    
    public function selectSubCategoriasAction() {
        $request = $this->get('request');
        $id_categoria = $request->request->get('categoria_id');
        $em = $this->getDoctrine()->getManager();

        $SubCategorias = $em->getRepository('MediadivAdminBundle:SubCategorias')->findBy(array ('categorias'=> $id_categoria ));

        return $this->render('MediadivAdminBundle:Default:selectCategorias.html.twig' , array (
            'data' => $SubCategorias,
        ));

    }
    
    public function guardarProductosAction(Request $request) {
        $session = $this->getRequest()->getSession();
        
        $em = $this->getDoctrine()->getManager();
        
        $nombre = $request->request->get('nombreProducto');
        $codigo = $request->request->get('codigoProducto');
        $descripcion = $request->request->get('descripcionProducto');
        $descripcionCorta = $request->request->get('descripcionCortaProducto');
        $caracteristicaUno = $request->request->get('caracteristicaUnoProducto');
        $caracteristicaDos = $request->request->get('caracteristicaDosProducto');
        $caracteristicaTres = $request->request->get('caracteristicaTresProducto');
        $stock = $request->request->get('sotckProducto');
        $estado = $request->request->get('estadoProducto');
        $subcategoriaid = $request->request->get('subcategoriaProducto');
        
        $subcategoria = $em->getRepository('MediadivAdminBundle:SubCategorias')->findOneBy(array('id'=>$subcategoriaid));
        
        $producto = new Productos();
        
        $producto->setNombre($nombre);
        $producto->setCodigo($codigo);
        $producto->setDescripcion($descripcion);
        $producto->setDescripcioncorta($descripcionCorta);
        $producto->setCaracteristicasuno($caracteristicaUno);
        $producto->setCaracteristicasdos($caracteristicaDos);
        $producto->setCaracteristicastres($caracteristicaTres);
        $producto->setStock($stock);
        $producto->setEstado($estado);
        $producto->setSubcategorias($subcategoria);
        $producto->setFechaIngreso(new \DateTime);
        
        $em->persist($producto);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
                'productosGuardado',
                'Se ha guardado '. $producto->getNombre() .' correctamente.'
                );
        
        $idProducto = $producto->getId();
        
        $session->set('idProducto', $idProducto);
        
        
        return $this->redirect($this->generateUrl('admin_productos_vistaUploadProducto'));
        
    }
    
    public function vistaUploadProductoAction() {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario'  => $nusuario , 'password' => $contrasena));
        
        
        if ($admin) {
        $idProducto = $session->get('idProducto');
        

        
        
        return $this->render('MediadivAdminBundle:Productos:vistaUploadProducto.html.twig', array('idProducto' => $idProducto));
                
        }else{
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista'));  
        }
        
    }
    
    public function uploadProductoAction() {
        
        $session = $this->getRequest()->getSession();
        $id = $session->get('idProducto');
        
        $idProducto = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'productos/' . $idProducto . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'productos/' . $idProducto . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    }
                    else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                }
                else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            }
            else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                }
                else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            }
            else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);


            
            $em = $this->getDoctrine()->getManager();
            $productos = $em->getRepository('MediadivAdminBundle:Productos')->findOneBy(array('id' => $idProducto));

            $fotoProducto = new FotosProductos();

           
            $fotoProducto->setFecha(new \DateTime());
            $fotoProducto->setProductos($productos);
            $fotoProducto->setUrl($fileName);

            $em->persist($productos);
            $em->persist($fotoProducto);
            $em->flush();
            
            $resp ='<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }
    
    public function elminaFotoProductoAction(Request $request) {
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();
        
        $fotoProducto = $em->getRepository('MediadivAdminBundle:FotosProductos')->find($id);
        
        $em->remove($fotoProducto);
        $em->flush();
        
        return new Response('eliminado');
    }
}
