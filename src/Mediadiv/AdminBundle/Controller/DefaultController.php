<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;
use Mediadiv\AdminBundle\Entity\Admin;
use Mediadiv\AdminBundle\Entity\Roles;
use Mediadiv\AdminBundle\Entity\Titles;
use Mediadiv\AdminBundle\Form\TitlesType;
use Mediadiv\AdminBundle\Entity\Tags;
use Mediadiv\AdminBundle\Form\TagsType;

class DefaultController extends FOSRestController {

    public function vistaUploadFotoPAction() {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Default:uploadFotoProducto.html.twig', array(
                    'rol' => $nombre,
                    'username' => $nombreusers,
                    'userid' => $user->getId()
        ));
    }

    public function selectCategoriasAction() {
        $request = $this->get('request');
        $id_categoria = $request->request->get('categoria_id');
        $em = $this->getDoctrine()->getManager();

        $SubCategorias = $em->getRepository('MediadivAdminBundle:SubCategorias')->findBy(array('categorias' => $id_categoria));




        return $this->render('MediadivAdminBundle:Default:selectCategorias.html.twig', array(
                    'data' => $SubCategorias,
        ));
    }

    public function AnalyticsAction() {

        $buzz = $this->container->get('buzz');

        $response = $buzz->get('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getType&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538');

        dump($response->getContent());




        $serializer = $this->container->get('serializer');
        $dispositivos = $serializer->serialize($response, 'json');



        $contenido = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getType&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);

       

        return $this->render('MediadivAdminBundle:Default:grafico.html.twig', array(
                    'dispositivos' => $contenido,
        ));
    }

    public function VistaUploadSliderAction($id) {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:Slider:vistaUploadFotoSlider.html.twig', array(
                    'id' => $id,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
                        )
        );
    }

    public function selectSubcategoriasAction() {

        return new Response(2);
    }

    public function testAction() {

        $em = $this->getDoctrine()->getManager();
        $productos = $em->getRepository('MediadivAdminBundle:Productos')->findAll();
        $categorias = $em->getRepository('MediadivAdminBundle:Categorias')->findAll();
        $fotos = $em->getRepository('MediadivAdminBundle:FotosProductos')->findAll();
        $SubCategorias = $em->getRepository('MediadivAdminBundle:SubCategorias')->findBy(array('categorias' => '3'));



        return $this->render('MediadivAdminBundle:Default:test.html.twig', array(
                    'productos' => $productos,
                    'SubCategorias' => $SubCategorias,
                    'categorias' => $categorias,
                    'fotos' => $fotos
        ));
    }

    public function checkAction() {


        $buzz = $this->container->get('buzz');

        $response = $buzz->get('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getType&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538');

        dump($response->getContent());




        $serializer = $this->container->get('serializer');
        $dispositivos = $serializer->serialize($response, 'json');



        $os = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getOsFamilies&idSite=1&period=day&date=yesterday&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);

        dump($os);

        return $this->render('MediadivAdminBundle:Default:principal.html.twig', array('os' => $os)
        );
    }

    public function loginAction() {
        $request = $this->getRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                    SecurityContext::AUTHENTICATION_ERROR
            );
        } else {
            $error = ('Usuario o password Errone');
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render(
                        'MediadivAdminBundle:Default:index.html.twig', array(
                    // last username entered by the user
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                        )
        );
    }

    public function dumpStringAction() {
        return $this->render('MediadivAdminBundle:Default:login.html.twig', array());
    }

    public function PrincipalAdminVistaAction() {

        $em = $this->getDoctrine()->getManager();


        dump($user = $this->get('security.context')->getToken()->getUser());
        dump($id = $user->getid());
        dump($rol = $user->getRoles());



        $os = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getOsFamilies&idSite=1&period=day&date=this%20Month&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        dump($os);


        $estaSemanaVisitas = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=VisitTime.getByDayOfWeek&idSite=1&period=day&date=this%20week&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);

        $totalDevisitasHoy = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=VisitsSummary.getVisits&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        $visitasPorPas = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=UserCountry.getCountry&idSite=1&period=day&date=this%20week&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        dump($visitasPorPas);
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();


        $tag = $em->getRepository('MediadivAdminBundle:Tags')->findOneBy(array('estado' => '1'));
        $title = $em->getRepository('MediadivAdminBundle:Titles')->findOneBy(array('estado' => '1'));
        $analytics = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->findOneBy(array('estado' => '1'));
        
        
        
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Default:principal.html.twig', array('nombrerol' => $nombrerol, 'os' => $os,
                    'estaSemanaVisitas' => $estaSemanaVisitas,
                    'totalDevisitasHoy' => $totalDevisitasHoy,
                    'visitasPorPas' => $visitasPorPas,
                    'analytics' => $analytics,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'tag' => $tag,
                    'title' => $title,
                    'userid' => $user->getId()
                        )
        );
    }

    public function indexAction($name) {
        return $this->render('MediadivAdminBundle:Default:index.html.twig', array('name' => $name));
    }


}
