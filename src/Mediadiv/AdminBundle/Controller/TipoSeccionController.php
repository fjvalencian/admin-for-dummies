<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoSeccion;
use Mediadiv\AdminBundle\Form\TipoSeccionType;

/**
 * TipoSeccion controller.
 *
 */
class TipoSeccionController extends Controller {

    /**
     * Lists all TipoSeccion entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entities = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findBy(array(), array('orden' => 'desc'));

        return $this->render('MediadivAdminBundle:TipoSeccion:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a new TipoSeccion entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new TipoSeccion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $entities = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findBy(array(), array('id' => 'DESC'));

            $this->get('session')->getFlashBag()->add(
                    'tiposeccion', 'Tu Tipo de Seccion se ha Creado'
            );


            return $this->render('MediadivAdminBundle:TipoSeccion:index.html.twig', array(
                        'entities' => $entities,
                        'rol' => $nombre,
                        'username' => $nombreuser,
                        'userid' => $user->getId()
            ));
        }

        return $this->render('MediadivAdminBundle:TipoSeccion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to create a TipoSeccion entity.
     *
     * @param TipoSeccion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoSeccion $entity) {
        $form = $this->createForm(new TipoSeccionType(), $entity, array(
            'action' => $this->generateUrl('admin_tiposeccion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear',
            'attr' => array('class' => 'btn btn-defaul')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new TipoSeccion entity.
     *
     */
    public function newAction() {

        $em = $this->getDoctrine()->getManager();

        $entity = new TipoSeccion();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:TipoSeccion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Finds and displays a TipoSeccion entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoSeccion')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoSeccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoSeccion:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Displays a form to edit an existing TipoSeccion entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entity = $em->getRepository('MediadivAdminBundle:TipoSeccion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoSeccion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoSeccion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to edit a TipoSeccion entity.
     *
     * @param TipoSeccion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(TipoSeccion $entity) {
        $form = $this->createForm(new TipoSeccionType(), $entity, array(
            'action' => $this->generateUrl('admin_tiposeccion_update', array('id' => $entity->getId())),
            'method' => 'post',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Edits an existing TipoSeccion entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoSeccion')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoSeccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);


        $em->flush();

        if ($editForm->isValid()) {
            $em->flush();


            $this->get('session')->getFlashBag()->add(
                    'tiposeccion', 'Tu Tipo de Seccion se ha editado'
            );

            return $this->redirect($this->generateUrl('admin_tiposeccion', array(
                                'id' => $id,
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
            )));
        }

        return $this->render('MediadivAdminBundle:TipoSeccion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Deletes a TipoSeccion entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:TipoSeccion')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoSeccion entity.');
        }

        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
                'tiposeccion', 'Tu Tipo de Seccion se ha eliminado'
        );


        return $this->redirect($this->generateUrl('admin_tiposeccion', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to delete a TipoSeccion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_tiposeccion_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn btn-defaul')))
                        ->getForm()
        ;
    }

    public function updateListaTipoSeccionAction(Request $request) {

        $data = $request->request->all();

        $params = array();
        $content = $this->get('request')->getContent();
        $orden = $request->request->get('nuevoOrden');
        $elementos = count($orden);
        $em = $this->getDoctrine()->getManager();

        for ($i = 0; $i < $elementos; $i++) {

            $lista = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array('id' => $orden[$i]['id']));
            $lista->setOrden($orden[$i]['orden']);
            $em->persist($lista);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'tiposeccion', 'la lista se ha actualizado'
            );
        }



        return new Response(100);
    }

}
