<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Tags;
use Mediadiv\AdminBundle\Form\TagsType;

/**
 * Tags controller.
 *
 */
class TagsController extends Controller {

    /**
     * Lists all Tags entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:Tags')->findAll();

        return $this->render('MediadivAdminBundle:Tags:index.html.twig', array(
                    'entities' => $entities,
        ));
    }

    /**
     * Creates a new Tags entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();


        $entity = new Tags();
        $descripcion = $request->get('descripcion');
        $estado = $request->get('estado');



        $entity->setDescripcion($descripcion);
        $entity->setEstado($estado);
        $entity->setFechaIngreso(new \DateTime());


        $em->persist($entity);
        $em->flush();


        $this->get('session')->getFlashBag()->add(
                'tag', 'Tu Tag se ha creado'
        );

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->redirect($this->generateUrl('PrincipalAdminVista', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to create a Tags entity.
     *
     * @param Tags $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tags $entity) {
        $form = $this->createForm(new TagsType(), $entity, array(
            'action' => $this->generateUrl('admin_tags_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Tags entity.
     *
     */
    public function newAction() {
        $entity = new Tags();
        $form = $this->createCreateForm($entity);

        return $this->render('MediadivAdminBundle:Tags:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Tags entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Tags')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tags entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Tags:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Tags entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Tags')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tags entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Tags:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Tags entity.
     *
     * @param Tags $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Tags $entity) {
        $form = $this->createForm(new TagsType(), $entity, array(
            'action' => $this->generateUrl('admin_tags_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Tags entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $formdescripcion = $request->get('descripcion');

        $entity = $em->getRepository('MediadivAdminBundle:Tags')->find($id);

        $entity->setDescripcion($formdescripcion);

        $em->merge($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'tag', 'Tu Tag se ha editado'
        );


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->redirect($this->generateUrl('PrincipalAdminVista', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Deletes a Tags entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:Tags')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tags entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_tags'));
    }

    /**
     * Creates a form to delete a Tags entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_tags_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
