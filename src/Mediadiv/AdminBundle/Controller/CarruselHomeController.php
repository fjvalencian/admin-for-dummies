<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\CarruselHome;
use Mediadiv\AdminBundle\Form\CarruselHomeType;

/**
 * CarruselHome controller.
 *
 */
class CarruselHomeController extends Controller {

    /**
     * Lists all CarruselHome entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:CarruselHome')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:CarruselHome:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a new CarruselHome entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new CarruselHome();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'carrusel', 'Tu carrusel se ha creado'
            );

            return $this->redirect($this->generateUrl('admin_carruselhome', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $userid
            )));
        }

        return $this->render('MediadivAdminBundle:CarruselHome:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a form to create a CarruselHome entity.
     *
     * @param CarruselHome $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CarruselHome $entity) {
        $form = $this->createForm(new CarruselHomeType(), $entity, array(
            'action' => $this->generateUrl('admin_carruselhome_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Displays a form to create a new CarruselHome entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = new CarruselHome();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:CarruselHome:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Finds and displays a CarruselHome entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:CarruselHome')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CarruselHome entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:CarruselHome:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CarruselHome entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:CarruselHome')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CarruselHome entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('MediadivAdminBundle:CarruselHome:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a form to edit a CarruselHome entity.
     *
     * @param CarruselHome $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CarruselHome $entity) {
        $form = $this->createForm(new CarruselHomeType(), $entity, array(
            'action' => $this->generateUrl('admin_carruselhome_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Edits an existing CarruselHome entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:CarruselHome')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CarruselHome entity.');
        }

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'carrusel', 'Tu carrusel se ha editado'
            );

            return $this->redirect($this->generateUrl('admin_carruselhome', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $userid
            )));
        }

        return $this->render('MediadivAdminBundle:CarruselHome:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Deletes a CarruselHome entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:CarruselHome')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CarruselHome entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'carrusel', 'Tu carrusel se ha eliminado'
        );


        return $this->redirect($this->generateUrl('admin_carruselhome', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $userid
        )));
    }

    /**
     * Creates a form to delete a CarruselHome entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_carruselhome_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
