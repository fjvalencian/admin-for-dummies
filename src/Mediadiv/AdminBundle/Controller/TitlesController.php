<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Titles;
use Mediadiv\AdminBundle\Form\TitlesType;

/**
 * Titles controller.
 *
 */
class TitlesController extends Controller {

    /**
     * Lists all Titles entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:Titles')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:Titles:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Creates a new Titles entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new Titles();

        $title = $request->get('title');
        $estado = $request->get('estado');


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entity->setTitle($title);
        $entity->setEstado($estado);
        $entity->setFechaIngreso(new \DateTime());

        $em->persist($entity);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
                'title', 'Tu título se ha creado'
        );

        return $this->redirect($this->generateUrl('PrincipalAdminVista', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to create a Titles entity.
     *
     * @param Titles $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Titles $entity) {
        $form = $this->createForm(new TitlesType(), $entity, array(
            'action' => $this->generateUrl('admin_titles_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear',
            'attr' => array('class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Titles entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $entity = new Titles();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:Titles:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Finds and displays a Titles entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Titles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Titles entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Titles:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Titles entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Titles')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Titles entity.');
        }

        $editForm = $this->createEditForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:Titles:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Creates a form to edit a Titles entity.
     *
     * @param Titles $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Titles $entity) {
        $form = $this->createForm(new TitlesType(), $entity, array(
            'action' => $this->generateUrl('admin_titles_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar',
            'attr' => array('class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Edits an existing Titles entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Titles')->find($id);



        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $formtitle = $request->get('title');

        $entity->setTitle($formtitle);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Titles entity.');
        }


        $em->merge($entity);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
                'title', 'Tu título se ha editado'
        );

        return $this->redirect($this->generateUrl('PrincipalAdminVista', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Deletes a Titles entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Titles')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Titles entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_titles', array(
                            'rol' => $nombre,
                            'username' => $nombreuser
        )));
    }

    /**
     * Creates a form to delete a Titles entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_titles_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
