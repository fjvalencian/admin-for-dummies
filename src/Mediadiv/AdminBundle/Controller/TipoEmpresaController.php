<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoEmpresa;
use Mediadiv\AdminBundle\Form\TipoEmpresaType;

/**
 * TipoEmpresa controller.
 *
 */
class TipoEmpresaController extends Controller
{

    /**
     * Lists all TipoEmpresa entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entities = $em->getRepository('MediadivAdminBundle:TipoEmpresa')->findAll();

        return $this->render('MediadivAdminBundle:TipoEmpresa:index.html.twig', array(
            'entities' => $entities,
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }
    /**
     * Creates a new TipoEmpresa entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoEmpresa();
        
        $titulo = $request->request->get('mediadiv_adminbundle_tipoempresa_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tipoempresa_descripcion');
        $posicionForm = $request->request->get('mediadiv_adminbundle_tipoempresa_posicion');
        $seccionForm = $request->request->get('mediadiv_adminbundle_tipoempresa_tiposeccion');

        $em = $this->getDoctrine()->getManager();

        $posicion = $em->getRepository('MediadivAdminBundle:Posicion')
                    ->findOneBy(array('id' => $posicionForm));

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                   ->findOneBy(array('id' => $seccionForm));

        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $entity->setPosicion($posicion);
        $entity->setTiposeccion($seccion);

        $em->persist($entity);
        $em->flush();

        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        
    }

    /**
     * Creates a form to create a TipoEmpresa entity.
     *
     * @param TipoEmpresa $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoEmpresa $entity)
    {
        $form = $this->createForm(new TipoEmpresaType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoempresa_create'),
            'method' => 'POST',
        ));

        #$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoEmpresa entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoEmpresa();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $home = $em->getRepository("MediadivAdminBundle:TipoSeccion")->findOneBy(array("titulo" => "HOME"));
        if($home){

            $idSeccionHome = $home->getId();

        }

        return $this->render('MediadivAdminBundle:TipoEmpresa:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    "idSeccionHome" => $idSeccionHome,
                    'userid' => $userid
        ));
    }

    /**
     * Finds and displays a TipoEmpresa entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEmpresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoEmpresa:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Displays a form to edit an existing TipoEmpresa entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEmpresa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoEmpresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
    * Creates a form to edit a TipoEmpresa entity.
    *
    * @param TipoEmpresa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoEmpresa $entity)
    {
        $form = $this->createForm(new TipoEmpresaType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoempresa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar' ,
                'attr' => array('class' => 'btn btn-success') ));

        return $form;
    }
    /**
     * Edits an existing TipoEmpresa entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoEmpresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEmpresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_tipoempresa_edit', 
                array(
                    'id' => $id,
                    'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid)));
        }

        return $this->render('MediadivAdminBundle:TipoEmpresa:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }
    /**
     * Deletes a TipoEmpresa entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

    
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:TipoEmpresa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoEmpresa entity.');
            }

            $em->remove($entity);
            $em->flush();
       

        return $this->redirect($this->generateUrl('admin_tipoempresa', array(
            'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
            )));
    }

    /**
     * Creates a form to delete a TipoEmpresa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tipoempresa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
