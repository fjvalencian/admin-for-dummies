<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\Session\Flash;

use Mediadiv\AdminBundle\Entity\SubCategorias;
use Mediadiv\AdminBundle\Entity\Admin;
use Mediadiv\AdminBundle\Form\SubCategoriasType;

/**
 * SubCategorias controller.
 *
 */
class SubCategoriasController extends Controller
{

    /**
     * Lists all SubCategorias entities.
     *
     */
    public function indexAction()
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        

            $entities = $em->getRepository('MediadivAdminBundle:SubCategorias')->findAll();

            return $this->render('MediadivAdminBundle:SubCategorias:index.html.twig', array(
                'entities' => $entities,
            ));
       
    }



    public function createAction(Request $request)
    {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
        
        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario'  => $nusuario , 'password' => $contrasena));
       
            $entity = new SubCategorias();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setFechaIngreso(new \DateTime());
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'subcategoria',
                        'Tu subcategoria a sido creada.'
                        );

                return $this->redirect($this->generateUrl('admin_subcategorias', array('id' => $entity->getId())));
            }

            return $this->render('MediadivAdminBundle:SubCategorias:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
        
        
        
    }

    /**
    * Creates a form to create a SubCategorias entity.
    *
    * @param SubCategorias $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(SubCategorias $entity)
    {
        $form = $this->createForm(new SubCategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_subcategorias_create'),
            'method' => 'POST',
        ));

        $form->add('Guardar', 'submit', array('attr' => array('class' => 'btn btn-default pull-left') ), array('label' => 'Guardar') );

        return $form;
    }

    /**
     * Displays a form to create a new SubCategorias entity.
     *
     */
    public function newAction()
    {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
       
        
        
            $entity = new SubCategorias();
            $form   = $this->createCreateForm($entity);

            return $this->render('MediadivAdminBundle:SubCategorias:new.html.twig', array(
                'entity' => $entity,
                'form'   => $form->createView(),
            ));
                
        
    }

    /**
     * Finds and displays a SubCategorias entity.
     *
     */
    public function showAction($id)
    {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
      
        
        $entity = $em->getRepository('MediadivAdminBundle:SubCategorias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubCategorias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:SubCategorias:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
     
    }

    /**
     * Displays a form to edit an existing SubCategorias entity.
     *
     */
    public function editAction($id)
    {

        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
      
            $entity = $em->getRepository('MediadivAdminBundle:SubCategorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubCategorias entity.');
            }

            $editForm = $this->createEditForm($entity);
            $deleteForm = $this->createDeleteForm($id);

            return $this->render('MediadivAdminBundle:SubCategorias:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
          
        
    }

    /**
    * Creates a form to edit a SubCategorias entity.
    *
    * @param SubCategorias $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SubCategorias $entity)
    {
        $form = $this->createForm(new SubCategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_subcategorias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        $form->add('Editar', 'submit', array('attr' => array('class' => 'btn btn-default pull-left') ), array('label' => 'Guardar') );


        return $form;
    }
    /**
     * Edits an existing SubCategorias entity.
     *
     */
    public function updateAction(Request $request, $id)
    {

        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        
        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');
     
            $entity = $em->getRepository('MediadivAdminBundle:SubCategorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubCategorias entity.');
            }

            $deleteForm = $this->createDeleteForm($id);
            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();
                $this->get('session')->getFlashBag()->add(
                        'subcategoria',
                        'Su subCategoría a sido editada correctamente'
                        );

                return $this->redirect($this->generateUrl('admin_subcategorias'));
            }

            return $this->render('MediadivAdminBundle:SubCategorias:edit.html.twig', array(
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ));
                
       
        
        
    }
    /**
     * Deletes a SubCategorias entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:SubCategorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubCategorias entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_subcategorias'));
    }



    public function delete3Action(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:SubCategorias')->find($id);



            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('admin_subcategorias'));
    }


    /**
     * Creates a form to delete a SubCategorias entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_subcategorias_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar', 'attr' => array('class' => 'btn btn-danger')))
            ->getForm()
        ;
    }
}
