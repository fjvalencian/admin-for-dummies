<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoCliente;
use Mediadiv\AdminBundle\Form\TipoClienteType;

/**
 * TipoCliente controller.
 *
 */
class TipoClienteController extends Controller
{

    /**
     * Lists all TipoCliente entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entities = $em->getRepository('MediadivAdminBundle:TipoCliente')->findAll();

        return $this->render('MediadivAdminBundle:TipoCliente:index.html.twig', array(
            'entities' => $entities,
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Creates a new TipoCliente entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoCliente();
        

        $titulo = $request->request->get('mediadiv_adminbundle_tipocliente_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tipocliente_descripcion');
        $posicionForm = $request->request->get('mediadiv_adminbundle_tipocliente_posicion');
        $seccionForm = $request->request->get('mediadiv_adminbundle_tipocliente_tiposeccion');




        $em = $this->getDoctrine()->getManager();

        $posicion = $em->getRepository('MediadivAdminBundle:Posicion')
                    ->findOneBy(array('id' => $posicionForm));

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                   ->findOneBy(array('id' => $seccionForm));

        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $entity->setPosicion($posicion);
        $entity->setTiposeccion($seccion);
        

        $em->persist($entity);
        $em->flush();

       
        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;     
        
    }

    /**
     * Creates a form to create a TipoCliente entity.
     *
     * @param TipoCliente $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoCliente $entity)
    {
        $form = $this->createForm(new TipoClienteType(), $entity, array(
            'action' => $this->generateUrl('admin_tipocliente_create'),
            'method' => 'POST',
        ));

        #$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoCliente entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoCliente();
        $form   = $this->createCreateForm($entity);
 
        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $home = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array("titulo" => "HOME"));
        $idSeccionHome = $home->getId();
   

        return $this->render('MediadivAdminBundle:TipoCliente:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'rol' => $nombrerol,
            "idSeccionHome" => $idSeccionHome ,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
     * Finds and displays a TipoCliente entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoCliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCliente entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoCliente:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
     * Displays a form to edit an existing TipoCliente entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoCliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCliente entity.');
        }

        $editForm = $this->createEditForm($entity);

        $home = $em->getRepository('MediadivAdminBundle:TipoSeccion')->findOneBy(array("titulo" => "HOME"));
        $idSeccionHome = $home->getId();

        

        return $this->render('MediadivAdminBundle:TipoCliente:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'rol' => $nombrerol,
            "idSeccionHome" => $idSeccionHome,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
    * Creates a form to edit a TipoCliente entity.
    *
    * @param TipoCliente $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoCliente $entity)
    {
        $form = $this->createForm(new TipoClienteType(), $entity, array(
            'action' => $this->generateUrl('admin_tipocliente_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar',
                'attr' => array('class' => 'btn btn-success') 

            ));

        return $form;
    }
    /**
     * Edits an existing TipoCliente entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoCliente')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoCliente entity.');
        }

        
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_tipocliente_edit', 
                array(
                    'id' => $id,
                    'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid)));
        }

        return $this->render('MediadivAdminBundle:TipoCliente:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Deletes a TipoCliente entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {



        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:TipoCliente')->findOneBy(array("id" => $id));

        $em->remove($entity);
        $em->flush();
        

        return $this->redirect($this->generateUrl('admin_tipocliente', 
            array(
                'rol' => $nombrerol,
                'username' => $nombreuser,
                'userid' => $userid)));
    }

    /**
     * Creates a form to delete a TipoCliente entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tipocliente_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
