<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\GoogleAnalytics;
use Mediadiv\AdminBundle\Form\GoogleAnalyticsType;

/**
 * GoogleAnalytics controller.
 *
 */
class GoogleAnalyticsController extends Controller {

    /**
     * Lists all GoogleAnalytics entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:GoogleAnalytics:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a new GoogleAnalytics entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new GoogleAnalytics();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();




        if ($form->isValid()) {
            $entity->setFechaIngreso(new \DateTime());
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'analytics', 'Tu seguimiento se ha creado'
            );
            return $this->redirect($this->generateUrl('admin_googleanalytics', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $userid
            )));
        }

        return $this->render('MediadivAdminBundle:GoogleAnalytics:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a form to create a GoogleAnalytics entity.
     *
     * @param GoogleAnalytics $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(GoogleAnalytics $entity) {
        $form = $this->createForm(new GoogleAnalyticsType(), $entity, array(
            'action' => $this->generateUrl('admin_googleanalytics_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear',
            'attr' => array(
                'class' => 'btn btn-success'
        )));

        return $form;
    }

    /**
     * Displays a form to create a new GoogleAnalytics entity.
     *
     */
    public function newAction() {
        $entity = new GoogleAnalytics();
        $form = $this->createCreateForm($entity);

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:GoogleAnalytics:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Finds and displays a GoogleAnalytics entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GoogleAnalytics entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:GoogleAnalytics:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing GoogleAnalytics entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GoogleAnalytics entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('MediadivAdminBundle:GoogleAnalytics:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a form to edit a GoogleAnalytics entity.
     *
     * @param GoogleAnalytics $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(GoogleAnalytics $entity) {
        $form = $this->createForm(new GoogleAnalyticsType(), $entity, array(
            'action' => $this->generateUrl('admin_googleanalytics_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar',
            'attr' => array(
                'class' => 'btn btn-success'
            )
        ));

        return $form;
    }

    /**
     * Edits an existing GoogleAnalytics entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GoogleAnalytics entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'analytics', 'Tu seguimiento se ha editado'
            );

            return $this->redirect($this->generateUrl('admin_googleanalytics', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $userid
            )));
        }

        return $this->render('MediadivAdminBundle:GoogleAnalytics:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Deletes a GoogleAnalytics entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        $entity = $em->getRepository('MediadivAdminBundle:GoogleAnalytics')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find GoogleAnalytics entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'analytics', 'Tu seguimiento se ha eliminado'
        );

        return $this->redirect($this->generateUrl('admin_googleanalytics', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $userid
        )));
    }

    /**
     * Creates a form to delete a GoogleAnalytics entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_googleanalytics_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function graficosAction() {

        $em = $this->getDoctrine()->getManager();



        $os = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getOsFamilies&idSite=1&period=day&date=this%20month&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        $visitasPorPagina = json_decode(file_get_contents("http://www.mediadiv.cl/piwik/index.php?module=API&method=Actions.getPageTitles&format=JSON&idSite=1&period=day&date=yesterday&expanded=1&token_auth=6abbdd061e7e2723672c153691717538&filter_limit=100"),true);
        $navegadores = json_decode(file_get_contents("http://www.mediadiv.cl/piwik/index.php?module=API&method=DevicesDetection.getBrowsers&format=JSON&idSite=1&period=day&date=this%20month&expanded=1&token_auth=6abbdd061e7e2723672c153691717538&filter_limit=10"),true);
        $estaSemanaVisitas = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=VisitTime.getByDayOfWeek&idSite=1&period=day&date=this%20month&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        $totalDevisitasHoy = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=VisitsSummary.getVisits&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        $visitasPorPas = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=UserCountry.getCountry&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);
        $estadisticaSemanal = json_decode(file_get_contents('http://www.mediadiv.cl/piwik/index.php?module=API&method=VisitTime.getByDayOfWeek&idSite=1&period=day&date=today&format=JSON&token_auth=6abbdd061e7e2723672c153691717538'), true);




        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:GoogleAnalytics:graficos.html.twig', array(
                    'os' => $os,
                    'estaSemanaVisitas' => $estaSemanaVisitas,
                    'totalDevisitasHoy' => $totalDevisitasHoy,
                    "visitasPorPagina" => $visitasPorPagina,
                    'visitasPorPas' => $visitasPorPas,
                    "navegadores" =>  $navegadores,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid,
                    'estadisticasemanal' => $estadisticaSemanal
        ));
    }

}
