<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Potencialidades;
use Mediadiv\AdminBundle\Form\PotencialidadesType;

/**
 * Potencialidades controller.
 *
 */
class PotencialidadesController extends Controller {

    /**
     * Lists all Potencialidades entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entities = $em->getRepository('MediadivAdminBundle:Potencialidades')->findAll();

        return $this->render('MediadivAdminBundle:Potencialidades:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a new Potencialidades entity.
     *
     */
    public function createAction(Request $request) {


        $em = $this->getDoctrine()->getManager();

        $entity = new Potencialidades();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'potencialidad', 'Tu potencialidad se ha creado'
            );

            return $this->redirect($this->generateUrl('admin_potencialidades', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
            )));
        }

        return $this->render('MediadivAdminBundle:Potencialidades:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to create a Potencialidades entity.
     *
     * @param Potencialidades $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Potencialidades $entity) {
        $form = $this->createForm(new PotencialidadesType(), $entity, array(
            'action' => $this->generateUrl('admin_potencialidades_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Displays a form to create a new Potencialidades entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = new Potencialidades();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Potencialidades:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Finds and displays a Potencialidades entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Potencialidades')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Potencialidades entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Potencialidades:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Potencialidades entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Potencialidades')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Potencialidades entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Potencialidades:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to edit a Potencialidades entity.
     *
     * @param Potencialidades $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Potencialidades $entity) {
        $form = $this->createForm(new PotencialidadesType(), $entity, array(
            'action' => $this->generateUrl('admin_potencialidades_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-defaul')));


        return $form;
    }

    /**
     * Edits an existing Potencialidades entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Potencialidades')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Potencialidades entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'potencialidad', 'Tu potencialidad se ha editado'
            );

            return $this->redirect($this->generateUrl('admin_potencialidades', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
            )));
        }

        return $this->render('MediadivAdminBundle:Potencialidades:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Deletes a Potencialidades entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Potencialidades')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Noticias entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'potencialidad', 'Tu potencialidad se ha elimido'
        );


        return $this->redirect($this->generateUrl('admin_potencialidades', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to delete a Potencialidades entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_potencialidades_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
