<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoEstadisticas;
use Mediadiv\AdminBundle\Form\TipoEstadisticasType;

/**
 * TipoEstadisticas controller.
 *
 */
class TipoEstadisticasController extends Controller
{

    /**
     * Lists all TipoEstadisticas entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entities = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->findAll();

        return $this->render('MediadivAdminBundle:TipoEstadisticas:index.html.twig', array(
            'entities' => $entities,
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Creates a new TipoEstadisticas entity.
     *
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new TipoEstadisticas();
        
        $titulo = $request->request->get('mediadiv_adminbundle_tipoestadisticas_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tipoestadisticas_descripcion');
        $posicionForm = $request->request->get('mediadiv_adminbundle_tipoestadisticas_posicion');
        $seccionForm = $request->request->get('mediadiv_adminbundle_tipoestadisticas_tiposeccion');

         $posicion = $em->getRepository('MediadivAdminBundle:Posicion')
                    ->findOneBy(array('id' => $posicionForm));

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                   ->findOneBy(array('id' => $seccionForm));

        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $entity->setPosicion($posicion);
        $entity->setTiposeccion($seccion);
        
        $em->persist($entity);
        $em->flush();

        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
            
    }

    /**
     * Creates a form to create a TipoEstadisticas entity.
     *
     * @param TipoEstadisticas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoEstadisticas $entity)
    {
        $form = $this->createForm(new TipoEstadisticasType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoestadisticas_create'),
            'method' => 'POST',
        ));

        #$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoEstadisticas entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoEstadisticas();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        return $this->render('MediadivAdminBundle:TipoEstadisticas:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
     * Finds and displays a TipoEstadisticas entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEstadisticas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoEstadisticas:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
     * Displays a form to edit an existing TipoEstadisticas entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();


        $entity = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->find($id);
        $seccionHome = $em->getRepository("MediadivAdminBundle:TipoSeccion")->findOneBy(array("titulo" => "HOME"));
        if($seccionHome){

            $idSeccionHome = $seccionHome->getId();

        }else{
            
            $idSeccionHome = 'null';

        }
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEstadisticas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoEstadisticas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            "idSeccionHome" => $idSeccionHome,
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
    * Creates a form to edit a TipoEstadisticas entity.
    *
    * @param TipoEstadisticas $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoEstadisticas $entity)
    {
        $form = $this->createForm(new TipoEstadisticasType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoestadisticas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar',
            'attr' => array('class' => 'btn btn-success')));

        return $form;
    }
    /**
     * Edits an existing TipoEstadisticas entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $entity = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoEstadisticas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_tipoestadisticas_edit', 
                array(
                    'id' => $id,
                    'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid)));
        }

        return $this->render('MediadivAdminBundle:TipoEstadisticas:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Deletes a TipoEstadisticas entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

       
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivAdminBundle:TipoEstadisticas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TipoEstadisticas entity.');
            }

            $em->remove($entity);
            $em->flush();
      

        return $this->redirect($this->generateUrl('admin_tipoestadisticas', array(
                'rol' => $nombrerol,
                'username' => $nombreuser,
                'userid' => $userid
            )));
    }

    /**
     * Creates a form to delete a TipoEstadisticas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tipoestadisticas_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
