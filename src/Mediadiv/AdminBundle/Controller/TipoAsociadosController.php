<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoAsociados;
use Mediadiv\AdminBundle\Form\TipoAsociadosType;

/**
 * TipoAsociados controller.
 *
 */
class TipoAsociadosController extends Controller
{

    /**
     * Lists all TipoAsociados entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();


        $entities = $em->getRepository('MediadivAdminBundle:TipoAsociados')->findAll();

        return $this->render('MediadivAdminBundle:TipoAsociados:index.html.twig', array(
            'entities' => $entities,
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Creates a new TipoAsociados entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TipoAsociados();

        $titulo = $request->request->get('mediadiv_adminbundle_tipoasociados_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tipoasociados_descripcion');
        $posicionForm = $request->request->get('mediadiv_adminbundle_tipoasociados_posicion');
        $seccionForm = $request->request->get('mediadiv_adminbundle_tipoasociados_tiposeccion');

       

        $em = $this->getDoctrine()->getManager();

        $posicion = $em->getRepository('MediadivAdminBundle:Posicion')
                    ->findOneBy(array('id' => $posicionForm));

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                   ->findOneBy(array('id' => $seccionForm));


        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $entity->setPosicion($posicion);
        $entity->setTiposeccion($seccion);

        $em->persist($entity);
        $em->flush();


        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;       
    }

    /**
     * Creates a form to create a TipoAsociados entity.
     *
     * @param TipoAsociados $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoAsociados $entity)
    {
        $form = $this->createForm(new TipoAsociadosType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoasociados_create'),
            'method' => 'POST',
        ));

        #$form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TipoAsociados entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoAsociados();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        return $this->render('MediadivAdminBundle:TipoAsociados:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
     * Finds and displays a TipoAsociados entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAsociados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAsociados entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoAsociados:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TipoAsociados entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAsociados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAsociados entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        return $this->render('MediadivAdminBundle:TipoAsociados:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }

    /**
    * Creates a form to edit a TipoAsociados entity.
    *
    * @param TipoAsociados $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoAsociados $entity)
    {
        $form = $this->createForm(new TipoAsociadosType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoasociados_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 
            'attr' => array(
                'class' => 'btn btn-success'
                )));

        return $form;
    }
    /**
     * Edits an existing TipoAsociados entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAsociados')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAsociados entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_tipoasociados_edit', 
                array(
                    'id' => $id,
                    'rol' => $nombrerol,
                    'username' => $nombreuser,
                    'userid' => $userid
                    )));
        }

        return $this->render('MediadivAdminBundle:TipoAsociados:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
        ));
    }
    /**
     * Deletes a TipoAsociados entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
      
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:TipoAsociados')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        

        $em->remove($entity);
        $em->flush();
        

        return $this->redirect($this->generateUrl('admin_tipoasociados', array( 
         'rol' => $nombrerol,
         'username' => $nombreuser,
         'userid' => $userid 
         )));
    }

    /**
     * Creates a form to delete a TipoAsociados entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_tipoasociados_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
