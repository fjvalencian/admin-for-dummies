<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Admin;
use Mediadiv\AdminBundle\Form\AdminType;
use Mediadiv\AdminBundle\Entity\Roles;

/**
 * Admin controller.
 *
 */
class AdminController extends Controller {

    /**
     * Lists all Admin entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:Admin')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Admin:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a new Admin entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $usuario = new Admin();



        $username = $request->get('username');
        $password = $request->get('password1');
        $correo = $request->get('correo');
        $rolid = $request->get('roles');
        $estado = $request->get('estado');


        $usuario->setUsername($username);

        $usuario->setSalt(md5(uniqid()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
        $usuario->setPassword($encoder->encodePassword($password, $usuario->getSalt()));
        $usuario->setIsActive($estado);
        $usuario->setEmail($correo);

        $roles = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('id' => $rolid));

        $usuario->setRoles($roles);

        $this->get('session')->getFlashBag()->add(
                'usuario', 'Tu estado se ha Creado'
        );


        $em->persist($usuario);
        $em->flush();



        return $this->redirect($this->generateUrl('admin_admin'));
    }

    /**
     * Creates a form to create a Admin entity.
     *
     * @param Admin $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Admin $entity) {
        $form = $this->createForm(new AdminType(), $entity, array(
            'action' => $this->generateUrl('admin_admin_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Admin entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();
        $roles = $em->getRepository('MediadivAdminBundle:Roles')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Admin:new.html.twig', array(
                    'roles' => $roles,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Finds and displays a Admin entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Admin')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Admin:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Admin entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Admin')->find($id);
        $roles = $em->getRepository('MediadivAdminBundle:Roles')->findAll();
        $rolActual = $entity->getRoles();


        $rolActual = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $rolActual));

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->render('MediadivAdminBundle:Admin:edit.html.twig', array(
                    'entity' => $entity,
                    'roles' => $roles,
                    'rolactual' => $rolActual,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $userid
        ));
    }

    /**
     * Creates a form to edit a Admin entity.
     *
     * @param Admin $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Admin $entity) {
        $form = $this->createForm(new AdminType(), $entity, array(
            'action' => $this->generateUrl('admin_admin_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Admin entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('MediadivAdminBundle:Admin')->find($id);

        $username = $request->get('username');
        $password = $request->get('password1');
        $correo = $request->get('correo');
        $rolid = $request->get('roles');
        $estado = $request->get('estado');

        $usuario->setUsername($username);

        $usuario->setSalt(md5(uniqid()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($usuario);
        $usuario->setPassword($encoder->encodePassword($password, $usuario->getSalt()));
        $usuario->setEmail($correo);
        $usuario->setIsActive($estado);

        $roles = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('id' => $rolid));

        $usuario->setRoles($roles);

        $em->persist($roles);
        $em->persist($usuario);

        $em->flush();

        //Para mandar el rol actual y los demas datos.
        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();
        
        $this->get('session')->getFlashBag()->add(
                'usuario', 'Tu estado se ha Editado'
        );
        



        return $this->redirect($this->generateUrl('admin_admin', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $userid
        )));
    }

    /**
     * Deletes a Admin entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Admin')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Admin entity.');
        }

        $em->remove($entity);
        $em->flush();
        
        $this->get('session')->getFlashBag()->add(
                'usuario', 'Tu estado se ha sido eliminado'
        );

        return $this->redirect($this->generateUrl('admin_admin', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $userid
                                )
        ));
    }

    /**
     * Creates a form to delete a Admin entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_admin_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
