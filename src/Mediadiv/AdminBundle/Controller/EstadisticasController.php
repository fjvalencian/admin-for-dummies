<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\AdminBundle\Entity\Estadisticas;
use Mediadiv\AdminBundle\Form\EstadisticasType;

/**
 * Estadisticas controller.
 *
 */
class EstadisticasController extends Controller {

    /**
     * Lists all Estadisticas entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:Estadisticas')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Estadisticas:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a new Estadisticas entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new Estadisticas();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'estadistica', 'Tu estadistica se ha creada'
            );

            return $this->redirect($this->generateUrl('admin_estadisticas', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
            )));
        }

        return $this->render('MediadivAdminBundle:Estadisticas:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to create a Estadisticas entity.
     *
     * @param Estadisticas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Estadisticas $entity) {
        $form = $this->createForm(new EstadisticasType(), $entity, array(
            'action' => $this->generateUrl('admin_estadisticas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Displays a form to create a new Estadisticas entity.
     *
     */
    public function newAction() {


        $em = $this->getDoctrine()->getManager();

        $entity = new Estadisticas();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Estadisticas:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Finds and displays a Estadisticas entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Estadisticas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estadisticas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Estadisticas:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Estadisticas entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Estadisticas')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estadisticas entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Estadisticas:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to edit a Estadisticas entity.
     *
     * @param Estadisticas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Estadisticas $entity) {
        $form = $this->createForm(new EstadisticasType(), $entity, array(
            'action' => $this->generateUrl('admin_estadisticas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Edits an existing Estadisticas entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entity = $em->getRepository('MediadivAdminBundle:Estadisticas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estadisticas entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'estadistica', 'Tu estadistica se ha editada'
            );

            return $this->redirect($this->generateUrl('admin_estadisticas', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
                                    )
            ));
        }

        return $this->render('MediadivAdminBundle:Estadisticas:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Deletes a Estadisticas entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        $entity = $em->getRepository('MediadivAdminBundle:Estadisticas')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Estadisticas entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'estadistica', 'Tu estadística se ha eliminada'
        );


        return $this->redirect($this->generateUrl('admin_estadisticas', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to delete a Estadisticas entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_estadisticas_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function vistaUploadEstAction($id) {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Estadisticas:uploadImagen.html.twig', array(
                    'id' => $id,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
                        )
        );
    }

    public function uploadEstadistAction($id) {



        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'estadisticas/' . $id . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
//   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'estadisticas/' . $id . '/';

//$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
// Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
// Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
// Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);
            $em = $this->getDoctrine()->getManager();
            $Estadisticas = $em->getRepository('MediadivAdminBundle:Estadisticas')->findOneBy(array('id' => $id));
            $Estadisticas->setImagen($fileName);
            $em->persist($Estadisticas);
            $em->flush();
        }



        return new Response(100);
    }

}
