<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\TipoAccordion;
use Mediadiv\AdminBundle\Form\TipoAccordionType;

/**
 * TipoAccordion controller.
 *
 */
class TipoAccordionController extends Controller
{

    /**
     * Lists all TipoAccordion entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivAdminBundle:TipoAccordion')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        dump($entities);

        return $this->render('MediadivAdminBundle:TipoAccordion:index.html.twig', array(
            'entities' => $entities,
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
            ));
    }
    /**
     * Creates a new TipoAccordion entity.
     *
     */
    public function createAction(Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = new TipoAccordion();
        
        $titulo  = $request->request->get('mediadiv_adminbundle_tipoaccordion_titulo');
        $descripcion = $request->request->get('mediadiv_adminbundle_tipoaccordion_descripcion');
        $posicionForm = $request->request->get('mediadiv_adminbundle_tipoaccordion_posicion');
        $seccionForm = $request->request->get('mediadiv_adminbundle_tipoaccordion_tiposeccion');

        $posicion = $em->getRepository('MediadivAdminBundle:Posicion')
                    ->findOneBy(array('id' => $posicionForm));

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')
                   ->findOneBy(array('id' => $seccionForm));

        $entity->setTitulo($titulo);
        $entity->setDescripcion($descripcion);
        $entity->setPosicion($posicion);
        $entity->setTiposeccion($seccion);

        $em->persist($entity);
        $em->flush();


        $response = new Response(json_encode(array('response' => 200)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
        
    }

    /**
     * Creates a form to create a TipoAccordion entity.
     *
     * @param TipoAccordion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TipoAccordion $entity)
    {
        $form = $this->createForm(new TipoAccordionType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoaccordion_create'),
            'method' => 'POST',
            ));

        #$form->add('submit', 'submit', array(
        #    'label' => 'Crear',
        #    'attr' => array('class' => 'btn btn-success')
        #    ));

        return $form;
    }

    /**
     * Displays a form to create a new TipoAccordion entity.
     *
     */
    public function newAction()
    {
        $entity = new TipoAccordion();
        $form   = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        return $this->render('MediadivAdminBundle:TipoAccordion:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
            ));
    }

    /**
     * Finds and displays a TipoAccordion entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAccordion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAccordion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:TipoAccordion:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            ));
    }

    /**
     * Displays a form to edit an existing TipoAccordion entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAccordion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAccordion entity.');
        }

        $editForm = $this->createEditForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        return $this->render('MediadivAdminBundle:TipoAccordion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
            ));
    }

    /**
    * Creates a form to edit a TipoAccordion entity.
    *
    * @param TipoAccordion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TipoAccordion $entity)
    {
        $form = $this->createForm(new TipoAccordionType(), $entity, array(
            'action' => $this->generateUrl('admin_tipoaccordion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            ));

        $form->add('submit', 'submit', array('label' => 'Editar',
            'attr' => array('class' => 'btn btn-success'))); 

        return $form;
    }
    /**
     * Edits an existing TipoAccordion entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:TipoAccordion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TipoAccordion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_tipoaccordion_edit', array('id' => $id)));
        }

        return $this->render('MediadivAdminBundle:TipoAccordion:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
            

            ));
    }
    /**
     * Deletes a TipoAccordion entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:TipoAccordion')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $userid = $user->getId();

        $em->remove($entity);
        $em->flush();
        

        return $this->redirect($this->generateUrl('admin_tipoaccordion', array(
            'rol' => $nombrerol,
            'username' => $nombreuser,
            'userid' => $userid
            )));
    }

    /**
     * Creates a form to delete a TipoAccordion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('admin_tipoaccordion_delete', array('id' => $id)))
        ->setMethod('DELETE')
        ->add('submit', 'submit', array('label' => 'Delete'))
        ->getForm()
        ;
    }
}
