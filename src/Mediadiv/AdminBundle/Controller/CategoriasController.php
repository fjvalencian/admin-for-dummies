<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\Categorias;
use Mediadiv\AdminBundle\Entity\FotoCategoria;
use Mediadiv\AdminBundle\Form\CategoriasType;
use Mediadiv\AdminBundle\Entity\Admin;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Categorias controller.
 *
 */
class CategoriasController extends Controller {

    /**
     * Lists all Categorias entities.
     *
     */
    public function indexAction() {

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('MediadivAdminBundle:Categorias')->findAll();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Categorias:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Creates a new Categorias entity.
     *
     */
    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $entity = new Categorias();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setFechaIngreso(new \DateTime());
            $em->persist($entity);
            $em->flush();

            $idCategoria = $entity->getId();
            $session->set('idCategoria', $idCategoria);

            $this->get('session')->getFlashBag()->add(
                    'categoria', 'Tu categoria a sido guardada'
            );

            return $this->redirect($this->generateUrl('admin_categorias_vistaUploadCategoria', array(
                                'rol' => $nombre,
                                'username' => $nombreuser
            )));
        }

        return $this->render('MediadivAdminBundle:Categorias:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Creates a form to create a Categorias entity.
     *
     * @param Categorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categorias $entity) {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_categorias_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Siguiente'));
        return $form;
    }

    /**
     * Displays a form to create a new Categorias entity.
     *
     */
    public function newAction() {

        $em = $this->getDoctrine()->getManager();

        $entity = new Categorias();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Categorias:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser
        ));
    }

    /**
     * Finds and displays a Categorias entity.
     *
     */
    public function showAction($id) {
        $session = $this->getRequest()->getSession();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Categorias')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorias entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Categorias:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing Categorias entity.
     *
     */
    public function editAction($id) {

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Categorias')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Categorias entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Categorias:edit.html.twig', array(
        'entity' => $entity,
        'edit_form' => $editForm->createView(),
        'delete_form' => $deleteForm->createView(),
        'rol' => $nombre,
        'username' => $nombreuser));
    }

    /**
     * Creates a form to edit a Categorias entity.
     *
     * @param Categorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Categorias $entity) {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_categorias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar'));

        return $form;
    }

    /**
     * Edits an existing Categorias entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();


        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('MediadivAdminBundle:Categorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Categorias entity.');
            }

            $deleteForm = $this->createDeleteForm($id);
            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();


                $idCategoria = $entity->getId();
                $session->set('idCategoria', $idCategoria);

                $this->get('session')->getFlashBag()->add(
                        'categoria', 'Tu categoria a sido guardada'
                );

                return $this->redirect($this->generateUrl('admin_categorias_vistaUploadCategoria', array(
                                    'rol' => $nombre,
                                    'username' => $nombreuser
                )));
            }

            return $this->render('MediadivAdminBundle:Categorias:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
                        'delete_form' => $deleteForm->createView(),
                        'rol' => $nombre,
                        'username' => $nombreuser
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista'));
        }
    }

    /**
     * Deletes a Categorias entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();


        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivAdminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if ($admin) {

            $form = $this->createDeleteForm($id);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity = $em->getRepository('MediadivAdminBundle:Categorias')->find($id);

                if (!$entity) {
                    throw $this->createNotFoundException('Unable to find Categorias entity.');
                }

                $em->remove($entity);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('admin_categorias', array(
                                'rol' => $nombre,
                                'username' => $nombreuser
            )));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('loginVista'));
        }
    }

    /**
     * Creates a form to delete a Categorias entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_categorias_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Eliminar', 'attr' => array('class' => 'btn btn-danger')))
                        ->getForm()
        ;
    }

    public function eliminarCategoriaAction($id) {



        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Categorias')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        return $this->redirect($this->generateUrl('admin_categorias', array(
                            'rol' => $nombre,
                            'username' => $nombreuser
        )));
    }

    public function vistaUploadCategoriaAction() {
        $session = $this->getRequest()->getSession();
        $idCategoria = $session->get('idCategoria');

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Categorias:vistaUploadCategoria.html.twig', 
                array( 
                    'idCategoria' => $idCategoria,
                    'rol' => $nombre,
                    'username' => $nombreuser
                ));
    }

    public function uploadCategoriaAction() {

        $session = $this->getRequest()->getSession();
        $id = $session->get('idCategoria');

        $idCategoria = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'categoria/' . $idCategoria . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'categoria/' . $idCategoria . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();
            $categoria = $em->getRepository('MediadivAdminBundle:Categorias')->findOneBy(array('id' => $idCategoria));

            $fotoCategoria = new FotoCategoria();

            $fotoCategoria->setCategorias($categoria);
            $fotoCategoria->setUrl($fileName);

            $em->persist($categoria);
            $em->persist($fotoCategoria);
            $em->flush();

            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

    public function eliminarFotoCategoriaAction(Request $request) {
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();

        $fotoCategoria = $em->getRepository('MediadivAdminBundle:FotoCategoria')->find($id);

        $em->remove($fotoCategoria);
        $em->flush();

        return new Response('eliminado');
    }

}
