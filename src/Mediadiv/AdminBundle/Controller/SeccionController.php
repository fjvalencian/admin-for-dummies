<?php

namespace Mediadiv\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\AdminBundle\Entity\Seccion;
use Mediadiv\AdminBundle\Form\SeccionType;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Seccion controller.
 *
 */
class SeccionController extends Controller {

    /**
     * Lists all Seccion entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entities = $em->getRepository('MediadivAdminBundle:Seccion')->findBy(array(), array('orden' => 'DESC'));

        return $this->render('MediadivAdminBundle:Seccion:index.html.twig', array(
                    'entities' => $entities,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a new Seccion entity.
     *
     */
    public function createAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        $entity = new Seccion();

        $titulo = $request->get('mediadiv_adminbundle_seccion_titulo');
        $contenido = $request->get('mediadiv_adminbundle_seccion_contenido');
        $siglas = $request->get('mediadiv_adminbundle_seccion_siglas');
        $idseccion = $request->get('mediadiv_adminbundle_seccion_tiposeccion');

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();


        $entity->setTitulo($titulo);
        $entity->setContenido($contenido);
        $entity->setSiglas($siglas);

        $seccion = $em->getRepository('MediadivAdminBundle:TipoSeccion')->find($idseccion);
        $entity->setTiposeccion($seccion);

        $em->persist($entity);
        $em->flush();
        $id = $entity->getId();
        $session = $this->getRequest()->getSession();
        $session->set('idSeccion', $id);

        $this->get('session')->getFlashBag()->add(
                'seccion', 'Tu contenido se ha creado'
        );

        $response = new Response(json_encode(array('response' => 200, 'idSeccion' => $id)));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Creates a form to create a Seccion entity.
     *
     * @param Seccion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Seccion $entity) {
        $form = $this->createForm(new SeccionType(), $entity, array(
            'action' => $this->generateUrl('admin_seccion_create'),
            'method' => 'POST',
        ));

        #$form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Displays a form to create a new Seccion entity.
     *
     */
    public function newAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = new Seccion();
        $form = $this->createCreateForm($entity);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Seccion:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Finds and displays a Seccion entity.
     *
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        $entity = $em->getRepository('MediadivAdminBundle:Seccion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Seccion:show.html.twig', array(
                    'entity' => $entity,
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Displays a form to edit an existing Seccion entity.
     *
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Seccion')->find($id);

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seccion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivAdminBundle:Seccion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Creates a form to edit a Seccion entity.
     *
     * @param Seccion $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Seccion $entity) {
        $form = $this->createForm(new SeccionType(), $entity, array(
            'action' => $this->generateUrl('admin_seccion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-defaul')));

        return $form;
    }

    /**
     * Edits an existing Seccion entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivAdminBundle:Seccion')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seccion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'seccion', 'Tu contenido se ha editado'
            );

            return $this->redirect($this->generateUrl('admin_seccion', array(
                                'rol' => $nombre,
                                'username' => $nombreuser,
                                'userid' => $user->getId()
            )));
        }

        return $this->render('MediadivAdminBundle:Seccion:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    /**
     * Deletes a Seccion entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivAdminBundle:Seccion')->find($id);


        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seccion entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'seccion', 'Tu contenido se ha eliminado'
        );


        return $this->redirect($this->generateUrl('admin_seccion', array(
                            'rol' => $nombre,
                            'username' => $nombreuser,
                            'userid' => $user->getId()
        )));
    }

    /**
     * Creates a form to delete a Seccion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('admin_seccion_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

    public function VistaUploadSeccionAction($id) {

        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $nombrerol = $user->getRoles();
        $nombreuser = $user->getUsername();
        $rol = $em->getRepository('MediadivAdminBundle:Roles')->findOneBy(array('nombre' => $nombrerol));
        $nombre = $rol->getNombre();

        return $this->render('MediadivAdminBundle:Seccion:uploadFoto.html.twig', array(
                    'id' => $id,
                    'rol' => $nombre,
                    'username' => $nombreuser,
                    'userid' => $user->getId()
        ));
    }

    public function UploadFotoSeccionAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $id = $session->get('idSeccion');


        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'seccion/' . $id . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'seccion/' . $id . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);

            $em = $this->getDoctrine()->getManager();

            $seccion = $em->getRepository('MediadivAdminBundle:Seccion')->findOneBy(array('id' => $id));
            $seccion->setImagen($fileName);

            $em->persist($seccion);
            $em->flush();

            $response = new Response($fileName);

            return $response;
        }



        return new Response(100);
    }

}
