<?php


namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\BrowserKit\Request;
/**
 * Admin
 *
 * @ORM\Table()
 * @ORM\Entity
 */

class Admin implements UserInterface, \Serializable {
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=25, unique=true)
	 */
	private $username;

	/**
	 * @ORM\Column(type="string", length=32)
	 */
	private $salt;

	/**
	 * @ORM\Column(type="string", length=40)
	 */
	private $password;

	/**
	 * @ORM\Column(type="string", length=60)
	 */
	private $email;

	/**
	 * @ORM\Column(name="is_active", type="boolean")
	 */
	private $isActive;



	 /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Roles", inversedBy="admin" )
     * @ORM\JoinColumn(name="roles_id", referencedColumnName="id")
     */


    private $roles;

    

	public function __construct() {
		$this->isActive = true;
		$this->salt     = md5(uniqid(null, true));
	}

	/**
	 * @inheritDoc
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @inheritDoc
	 */
	public function getSalt() {
		return $this->salt;
	}

	/**
	 * @inheritDoc
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\Roles 
     */

	
	public function getRoles() {

           

		return array($this->roles->getNombre());


		
       

    }

	/**
	 * @inheritDoc
	 */
	public function eraseCredentials() {
	}

	/**
	 * @see \Serializable::serialize()
	 */
	public function serialize() {
		return serialize(array(
				$this->id,
			));
	}

	/**
	 * @see \Serializable::unserialize()
	 */
	public function unserialize($serialized) {
		list(
			$this->id,
		) = unserialize($serialized);
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set username
	 *
	 * @param string $username
	 * @return Admin
	 */
	public function setUsername($username) {
		$this->username = $username;

		return $this;
	}

	/**
	 * Set salt
	 *
	 * @param string $salt
	 * @return Admin
	 */
	public function setSalt($salt) {
		$this->salt = $salt;

		return $this;
	}

	/**
	 * Set password
	 *
	 * @param string $password
	 * @return Admin
	 */
	public function setPassword($password) {
		$this->password = $password;

		return $this;
	}

	/**
	 * Set email
	 *
	 * @param string $email
	 * @return Admin
	 */
	public function setEmail($email) {
		$this->email = $email;

		return $this;
	}

	/**
	 * Get email
	 *
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Set isActive
	 *
	 * @param boolean $isActive
	 * @return Admin
	 */
	public function setIsActive($isActive) {
		$this->isActive = $isActive;

		return $this;
	}

	/**
	 * Get isActive
	 *
	 * @return boolean
	 */
	public function getIsActive() {
		return $this->isActive;
	}

    /**
     * Set roles
     *
     * @param \Mediadiv\AdminBundle\Entity\Roles $roles
     * @return Admin
     */
    public function setRoles(\Mediadiv\AdminBundle\Entity\Roles $roles = null)
    {
        $this->roles = $roles;

        return $this;
    }
}
