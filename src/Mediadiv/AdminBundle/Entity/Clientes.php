<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Clientes
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\ClientesRepository")
 */
class Clientes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="profesion", type="string", length=255)
     */
    private $profesion;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="text")
     */
    private $texto;


     /**
     * @var string
     *
     * @ORM\Column(name="foto", type="text" , nullable=true)
     */
    

    private $foto;


 
     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoCliente", inversedBy="clientes" )
     * @ORM\JoinColumn(name="tipocliente_id", referencedColumnName="id")
     */
  

    private $tipocliente;


    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Clientes
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set profesion
     *
     * @param string $profesion
     * @return Clientes
     */
    public function setProfesion($profesion)
    {
        $this->profesion = $profesion;

        return $this;
    }

    /**
     * Get profesion
     *
     * @return string 
     */
    public function getProfesion()
    {
        return $this->profesion;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Clientes
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Clientes
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set tipocliente
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente
     * @return Clientes
     */
    public function setTipocliente(\Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente = null)
    {
        $this->tipocliente = $tipocliente;

        return $this;
    }

    /**
     * Get tipocliente
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoCliente 
     */
    public function getTipocliente()
    {
        return $this->tipocliente;
    }
}
