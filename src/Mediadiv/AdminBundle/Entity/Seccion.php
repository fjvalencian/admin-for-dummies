<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seccion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\SeccionRepository")
 */
class Seccion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="Contenido", type="text")
     */
    private $contenido;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255 ,nullable=true)
     */
    private $imagen;


    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer" ,nullable=true )
     */


    private $orden;
  



     /**
     * @var string
     *
     * @ORM\Column(name="siglas", type="string", length=255 ,nullable=true)
     */

    private $siglas;


       /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="seccion" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */


    private $tiposeccion;

    



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Seccion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Seccion
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Seccion
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Seccion
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set siglas
     *
     * @param string $siglas
     * @return Seccion
     */
    public function setSiglas($siglas)
    {
        $this->siglas = $siglas;

        return $this;
    }

    /**
     * Get siglas
     *
     * @return string 
     */
    public function getSiglas()
    {
        return $this->siglas;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return Seccion
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }
}
