<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CarruselHome
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\CarruselHomeRepository")
 */
class CarruselHome
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     */
    private $contenido;


     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoAccordion", inversedBy="carruselhome" )
     * @ORM\JoinColumn(name="tipoaccordion_id", referencedColumnName="id")
     */


    private $tipoaccordion;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return CarruselHome
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return CarruselHome
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set tipoaccordion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion
     * @return CarruselHome
     */
    public function setTipoaccordion(\Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion = null)
    {
        $this->tipoaccordion = $tipoaccordion;

        return $this;
    }

    /**
     * Get tipoaccordion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoAccordion 
     */
    public function getTipoaccordion()
    {
        return $this->tipoaccordion;
    }
}
