<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotosProductos
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FotosProductos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /** @ORM\Column(type="datetime") */

    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=100 , nullable=true)
     */

    private $estado;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=100 , nullable=true)
     */

    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Productos", inversedBy="fotosproductos")
     * @ORM\JoinColumn(name="productos_id", referencedColumnName="id")
     *
     */




    private $productos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return FotosProductos
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return FotosProductos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FotosProductos
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set productos
     *
     * @param \Mediadiv\AdminBundle\Entity\Productos $productos
     * @return FotosProductos
     */
    public function setProductos(\Mediadiv\AdminBundle\Entity\Productos $productos = null)
    {
        $this->productos = $productos;

        return $this;
    }

    /**
     * Get productos
     *
     * @return \Mediadiv\AdminBundle\Entity\Productos 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
