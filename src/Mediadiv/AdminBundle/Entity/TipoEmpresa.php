<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoEmpresa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\TipoEmpresaRepository")
 */
class TipoEmpresa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;


     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="tipoempresa" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */

    private $tiposeccion;

       /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="tipoempresa" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */

        
    private $posicion; 

   
   /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Empresas", mappedBy="tipoempresa" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $empresas;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->empresas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoEmpresa
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoEmpresa
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return TipoEmpresa
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return TipoEmpresa
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Add empresas
     *
     * @param \Mediadiv\AdminBundle\Entity\Empresas $empresas
     * @return TipoEmpresa
     */
    public function addEmpresa(\Mediadiv\AdminBundle\Entity\Empresas $empresas)
    {
        $this->empresas[] = $empresas;

        return $this;
    }

    /**
     * Remove empresas
     *
     * @param \Mediadiv\AdminBundle\Entity\Empresas $empresas
     */
    public function removeEmpresa(\Mediadiv\AdminBundle\Entity\Empresas $empresas)
    {
        $this->empresas->removeElement($empresas);
    }

    /**
     * Get empresas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmpresas()
    {
        return $this->empresas;
    }
}
