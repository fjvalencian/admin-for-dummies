<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoAccordion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\TipoAccordionRepository")
 */
class TipoAccordion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="tipoaccordion" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */


     private $tiposeccion;

   /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="tipoaccordion" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */


   private $posicion; 

    



       /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\CarruselHome", mappedBy="tipoaccordion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"}) 
     */


       private $carruselhome;




       /**
     * Constructor
     */
    public function __construct()
    {
        $this->carruselhome = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoAccordion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoAccordion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return TipoAccordion
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return TipoAccordion
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Add carruselhome
     *
     * @param \Mediadiv\AdminBundle\Entity\CarruselHome $carruselhome
     * @return TipoAccordion
     */
    public function addCarruselhome(\Mediadiv\AdminBundle\Entity\CarruselHome $carruselhome)
    {
        $this->carruselhome[] = $carruselhome;

        return $this;
    }

    /**
     * Remove carruselhome
     *
     * @param \Mediadiv\AdminBundle\Entity\CarruselHome $carruselhome
     */
    public function removeCarruselhome(\Mediadiv\AdminBundle\Entity\CarruselHome $carruselhome)
    {
        $this->carruselhome->removeElement($carruselhome);
    }

    /**
     * Get carruselhome
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCarruselhome()
    {
        return $this->carruselhome;
    }
}
