<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoEstadisticas
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\TipoEstadisticasRepository")
 */
class TipoEstadisticas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="tipoestadisticas" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */


    private $tiposeccion;





       /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="tipoestadisticas" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */

        
    private $posicion;




    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Estadisticas", mappedBy="tipoestadisticas" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

     private $estadisticas;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->estadisticas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoEstadisticas
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoEstadisticas
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return TipoEstadisticas
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return TipoEstadisticas
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Add estadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\Estadisticas $estadisticas
     * @return TipoEstadisticas
     */
    public function addEstadistica(\Mediadiv\AdminBundle\Entity\Estadisticas $estadisticas)
    {
        $this->estadisticas[] = $estadisticas;

        return $this;
    }

    /**
     * Remove estadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\Estadisticas $estadisticas
     */
    public function removeEstadistica(\Mediadiv\AdminBundle\Entity\Estadisticas $estadisticas)
    {
        $this->estadisticas->removeElement($estadisticas);
    }

    /**
     * Get estadisticas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstadisticas()
    {
        return $this->estadisticas;
    }
}
