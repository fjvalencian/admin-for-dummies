<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotoCategoria
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FotoCategoria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Categorias", inversedBy="fotocategoria")
     * @ORM\JoinColumn(name="categorias_id", referencedColumnName="id")
     *
     */

    private $categorias;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FotoCategoria
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set categorias
     *
     * @param \Mediadiv\AdminBundle\Entity\Categorias $categorias
     * @return FotoCategoria
     */
    public function setCategorias(\Mediadiv\AdminBundle\Entity\Categorias $categorias = null)
    {
        $this->categorias = $categorias;

        return $this;
    }

    /**
     * Get categorias
     *
     * @return \Mediadiv\AdminBundle\Entity\Categorias 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }
}
