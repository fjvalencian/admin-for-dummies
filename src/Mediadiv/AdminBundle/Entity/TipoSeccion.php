<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoSeccion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\TipoSeccionRepository")
 */
class TipoSeccion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   
    
     /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;


     /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;



    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="tipoplantilla", type="text")
     */
    private $tipoplantilla;



    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Seccion", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"}) 
     */

    private $seccion;


     /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoAccordion", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"}) 
     */


    private $tipoaccordion;

    
     /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoEstadisticas", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"}) 
     */


    private $tipoestadisticas;
    


       /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoEmpresa", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"}) 
     */


    private $tipoempresa;



       /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoCliente", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $tipocliente;



    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoAsociados", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $tipoasociados;


       /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Slider", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $slider;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Noticias", mappedBy="tiposeccion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $noticias;


     /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer" ,nullable=true )
     */


    private $orden;

  
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seccion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoaccordion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoestadisticas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoempresa = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipocliente = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoasociados = new \Doctrine\Common\Collections\ArrayCollection();
        $this->slider = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noticias = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoSeccion
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return TipoSeccion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoSeccion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipoplantilla
     *
     * @param string $tipoplantilla
     * @return TipoSeccion
     */
    public function setTipoplantilla($tipoplantilla)
    {
        $this->tipoplantilla = $tipoplantilla;

        return $this;
    }

    /**
     * Get tipoplantilla
     *
     * @return string 
     */
    public function getTipoplantilla()
    {
        return $this->tipoplantilla;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return TipoSeccion
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Add seccion
     *
     * @param \Mediadiv\AdminBundle\Entity\Seccion $seccion
     * @return TipoSeccion
     */
    public function addSeccion(\Mediadiv\AdminBundle\Entity\Seccion $seccion)
    {
        $this->seccion[] = $seccion;

        return $this;
    }

    /**
     * Remove seccion
     *
     * @param \Mediadiv\AdminBundle\Entity\Seccion $seccion
     */
    public function removeSeccion(\Mediadiv\AdminBundle\Entity\Seccion $seccion)
    {
        $this->seccion->removeElement($seccion);
    }

    /**
     * Get seccion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * Add tipoaccordion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion
     * @return TipoSeccion
     */
    public function addTipoaccordion(\Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion)
    {
        $this->tipoaccordion[] = $tipoaccordion;

        return $this;
    }

    /**
     * Remove tipoaccordion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion
     */
    public function removeTipoaccordion(\Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion)
    {
        $this->tipoaccordion->removeElement($tipoaccordion);
    }

    /**
     * Get tipoaccordion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoaccordion()
    {
        return $this->tipoaccordion;
    }

    /**
     * Add tipoestadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas
     * @return TipoSeccion
     */
    public function addTipoestadistica(\Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas)
    {
        $this->tipoestadisticas[] = $tipoestadisticas;

        return $this;
    }

    /**
     * Remove tipoestadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas
     */
    public function removeTipoestadistica(\Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas)
    {
        $this->tipoestadisticas->removeElement($tipoestadisticas);
    }

    /**
     * Get tipoestadisticas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoestadisticas()
    {
        return $this->tipoestadisticas;
    }

    /**
     * Add tipoempresa
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa
     * @return TipoSeccion
     */
    public function addTipoempresa(\Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa)
    {
        $this->tipoempresa[] = $tipoempresa;

        return $this;
    }

    /**
     * Remove tipoempresa
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa
     */
    public function removeTipoempresa(\Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa)
    {
        $this->tipoempresa->removeElement($tipoempresa);
    }

    /**
     * Get tipoempresa
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoempresa()
    {
        return $this->tipoempresa;
    }

    /**
     * Add tipocliente
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente
     * @return TipoSeccion
     */
    public function addTipocliente(\Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente)
    {
        $this->tipocliente[] = $tipocliente;

        return $this;
    }

    /**
     * Remove tipocliente
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente
     */
    public function removeTipocliente(\Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente)
    {
        $this->tipocliente->removeElement($tipocliente);
    }

    /**
     * Get tipocliente
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipocliente()
    {
        return $this->tipocliente;
    }

    /**
     * Add tipoasociados
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados
     * @return TipoSeccion
     */
    public function addTipoasociado(\Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados)
    {
        $this->tipoasociados[] = $tipoasociados;

        return $this;
    }

    /**
     * Remove tipoasociados
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados
     */
    public function removeTipoasociado(\Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados)
    {
        $this->tipoasociados->removeElement($tipoasociados);
    }

    /**
     * Get tipoasociados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoasociados()
    {
        return $this->tipoasociados;
    }

    /**
     * Add slider
     *
     * @param \Mediadiv\AdminBundle\Entity\Slider $slider
     * @return TipoSeccion
     */
    public function addSlider(\Mediadiv\AdminBundle\Entity\Slider $slider)
    {
        $this->slider[] = $slider;

        return $this;
    }

    /**
     * Remove slider
     *
     * @param \Mediadiv\AdminBundle\Entity\Slider $slider
     */
    public function removeSlider(\Mediadiv\AdminBundle\Entity\Slider $slider)
    {
        $this->slider->removeElement($slider);
    }

    /**
     * Get slider
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * Add noticias
     *
     * @param \Mediadiv\AdminBundle\Entity\Noticias $noticias
     * @return TipoSeccion
     */
    public function addNoticia(\Mediadiv\AdminBundle\Entity\Noticias $noticias)
    {
        $this->noticias[] = $noticias;

        return $this;
    }

    /**
     * Remove noticias
     *
     * @param \Mediadiv\AdminBundle\Entity\Noticias $noticias
     */
    public function removeNoticia(\Mediadiv\AdminBundle\Entity\Noticias $noticias)
    {
        $this->noticias->removeElement($noticias);
    }

    /**
     * Get noticias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNoticias()
    {
        return $this->noticias;
    }
}
