<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoAsociados
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\TipoAsociadosRepository")
 */
class TipoAsociados
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Asociados", mappedBy="tipoasociados" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $asociados;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="tipoasociados" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */


    private $tiposeccion;
  


      /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="tipoasociados" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */

        
    private $posicion;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->asociados = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoAsociados
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoAsociados
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Add asociados
     *
     * @param \Mediadiv\AdminBundle\Entity\Asociados $asociados
     * @return TipoAsociados
     */
    public function addAsociado(\Mediadiv\AdminBundle\Entity\Asociados $asociados)
    {
        $this->asociados[] = $asociados;

        return $this;
    }

    /**
     * Remove asociados
     *
     * @param \Mediadiv\AdminBundle\Entity\Asociados $asociados
     */
    public function removeAsociado(\Mediadiv\AdminBundle\Entity\Asociados $asociados)
    {
        $this->asociados->removeElement($asociados);
    }

    /**
     * Get asociados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAsociados()
    {
        return $this->asociados;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return TipoAsociados
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return TipoAsociados
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }
}
