<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Empresas
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Empresas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255 ,nullable=true  )
     */
    private $imagen;

     
       /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoEmpresa", inversedBy="empresas" )
     * @ORM\JoinColumn(name="tipoempresa_id", referencedColumnName="id")
     */


    private $tipoempresa;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Empresas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Empresas
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set tipoempresa
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa
     * @return Empresas
     */
    public function setTipoempresa(\Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa = null)
    {
        $this->tipoempresa = $tipoempresa;

        return $this;
    }

    /**
     * Get tipoempresa
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoEmpresa 
     */
    public function getTipoempresa()
    {
        return $this->tipoempresa;
    }
}
