<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubCategorias
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SubCategorias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=255 , nullable=true)
     */

    private $nombre;



    /**
     * @var date
     * @ORM\Column(name="fechaIngreso", type="date",nullable=true)
     */




    private $fechaIngreso;




    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Categorias", inversedBy="subcategorias" )
     * @ORM\JoinColumn(name="categorias_id", referencedColumnName="id")
     */

    private $categorias;






    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Productos", mappedBy="subcategorias" , cascade={"remove"})
     */

    private $productos;



    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return SubCategorias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return SubCategorias
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set categorias
     *
     * @param \Mediadiv\AdminBundle\Entity\Categorias $categorias
     * @return SubCategorias
     */
    public function setCategorias(\Mediadiv\AdminBundle\Entity\Categorias $categorias = null)
    {
        $this->categorias = $categorias;

        return $this;
    }

    /**
     * Get categorias
     *
     * @return \Mediadiv\AdminBundle\Entity\Categorias 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add productos
     *
     * @param \Mediadiv\AdminBundle\Entity\Productos $productos
     * @return SubCategorias
     */
    public function addProducto(\Mediadiv\AdminBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Mediadiv\AdminBundle\Entity\Productos $productos
     */
    public function removeProducto(\Mediadiv\AdminBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
