<?php

namespace Mediadiv\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Slider
 *
 * @ORM\Table()
 * @ORM\Entity
 */

class Slider {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 * @ORM\Column(name="titulo", type="string", length=255 , nullable=true)
	 */

	private $titulo;

	/**
	 * @var string
	 * @ORM\Column(name="bajadatexto", type="string", length=255 , nullable=true)
	 */

	private $bajadatexto;
        
        
        /**
	 * @var string
	 * @ORM\Column(name="vermas", type="string", length=255 , nullable=true)
	 */

	private $vermas;
        
        /**
	 * @var string
	 * @ORM\Column(name="target", type="string", length=255 , nullable=true)
	 */

	private $target;
        
        

	/**
	 * @var string
	 * @ORM\Column(name="fotoslider", type="string", length=255 , nullable=true)
	 */

	private $fotoslider;



     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="slider" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */

    

    private $tiposeccion;
    
    
     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="slider" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */

        
    private $posicion;  



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Slider
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set bajadatexto
     *
     * @param string $bajadatexto
     * @return Slider
     */
    public function setBajadatexto($bajadatexto)
    {
        $this->bajadatexto = $bajadatexto;

        return $this;
    }

    /**
     * Get bajadatexto
     *
     * @return string 
     */
    public function getBajadatexto()
    {
        return $this->bajadatexto;
    }

    /**
     * Set vermas
     *
     * @param string $vermas
     * @return Slider
     */
    public function setVermas($vermas)
    {
        $this->vermas = $vermas;

        return $this;
    }

    /**
     * Get vermas
     *
     * @return string 
     */
    public function getVermas()
    {
        return $this->vermas;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return Slider
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set fotoslider
     *
     * @param string $fotoslider
     * @return Slider
     */
    public function setFotoslider($fotoslider)
    {
        $this->fotoslider = $fotoslider;

        return $this;
    }

    /**
     * Get fotoslider
     *
     * @return string 
     */
    public function getFotoslider()
    {
        return $this->fotoslider;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return Slider
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return Slider
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }
}
