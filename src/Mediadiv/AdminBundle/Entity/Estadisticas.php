<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estadisticas
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Estadisticas
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="cantidad", type="integer")
     */
    private $cantidad;



    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255)
     */


    private $icon;



     /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255 ,nullable=true )
     */
    private $imagen;



     
      /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoEstadisticas", inversedBy="estadisticas" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */

    

    private $tipoestadisticas;

     

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Estadisticas
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set cantidad
     *
     * @param integer $cantidad
     * @return Estadisticas
     */
    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;

        return $this;
    }

    /**
     * Get cantidad
     *
     * @return integer 
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Estadisticas
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Estadisticas
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set tipoestadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas
     * @return Estadisticas
     */
    public function setTipoestadisticas(\Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas = null)
    {
        $this->tipoestadisticas = $tipoestadisticas;

        return $this;
    }

    /**
     * Get tipoestadisticas
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoEstadisticas 
     */
    public function getTipoestadisticas()
    {
        return $this->tipoestadisticas;
    }
}
