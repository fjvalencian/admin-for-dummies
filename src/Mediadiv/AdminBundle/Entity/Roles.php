<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Roles
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Roles
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;
    

     /**
     * @var string
     *
     * @ORM\Column(name="nombrerol", type="string", length=255)
     */
    private $nombrerol;


     /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Admin", mappedBy="roles" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $admin;
 
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->admin = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Roles
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nombrerol
     *
     * @param string $nombrerol
     * @return Roles
     */
    public function setNombrerol($nombrerol)
    {
        $this->nombrerol = $nombrerol;

        return $this;
    }

    /**
     * Get nombrerol
     *
     * @return string 
     */
    public function getNombrerol()
    {
        return $this->nombrerol;
    }

    /**
     * Add admin
     *
     * @param \Mediadiv\AdminBundle\Entity\Admin $admin
     * @return Roles
     */
    public function addAdmin(\Mediadiv\AdminBundle\Entity\Admin $admin)
    {
        $this->admin[] = $admin;

        return $this;
    }

    /**
     * Remove admin
     *
     * @param \Mediadiv\AdminBundle\Entity\Admin $admin
     */
    public function removeAdmin(\Mediadiv\AdminBundle\Entity\Admin $admin)
    {
        $this->admin->removeElement($admin);
    }

    /**
     * Get admin
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAdmin()
    {
        return $this->admin;
    }
}
