<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productos
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Productos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=255 , nullable=true)
     */


    private $nombre;



    /**
     * @var string
     * @ORM\Column(name="codigo", type="string", length=255 , nullable=true)
     */
    private $codigo;


    /**
     * @var string
     * @ORM\Column(name="descripcion", type="string", length=400 , nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     * @ORM\Column(name="descripcioncorta", type="string", length=255 , nullable=true)
     */

    private $descripcioncorta;

    /**
     * @var string
     * @ORM\Column(name="caracteristicasuno", type="string", length=400 , nullable=true)
     */
    private $caracteristicasuno;

    /**
     * @var string
     * @ORM\Column(name="caracteristicasdos", type="string", length=400 , nullable=true)
     */
    private $caracteristicasdos;
    /**
     * @var string
     * @ORM\Column(name="caracteristicastres", type="string", length=400 , nullable=true)
     */
    private $caracteristicastres;
    /**
     * @var string
     * @ORM\Column(name="caracteristicascuatro", type="string", length=400 , nullable=true)
     */
    private $caracteristicascuatro;
    /**
     * @var string
     * @ORM\Column(name="caracteristicascinco", type="string", length=400 , nullable=true)
     */
    private $caracteristicascinco;
    /**
     * @var string
     * @ORM\Column(name="caracteristicasseis", type="string", length=400 , nullable=true)
     */
    private $caracteristicasseis;

    /**
     * @var string
     * @ORM\Column(name="stock", type="string", length=400 , nullable=true)
     */

    private $stock;
    /**
     * @var string
     * @ORM\Column(name="estado", type="string", length=400 , nullable=true)
     */

    private $estado;



    /**
     * @var string
     *
     * @ORM\Column(name="fechaIngreso", type="date", nullable=true)
     */

    private $fechaIngreso;




    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\FotosProductos", mappedBy="productos" , cascade={"remove"})
     */




    private $fotosproductos;


    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\SubCategorias", inversedBy="productos" )
     * @ORM\JoinColumn(name="subcategorias_id", referencedColumnName="id")
     */



    private $subcategorias;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fotosproductos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Productos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set codigo
     *
     * @param string $codigo
     * @return Productos
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return string
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Productos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set descripcioncorta
     *
     * @param string $descripcioncorta
     * @return Productos
     */
    public function setDescripcioncorta($descripcioncorta)
    {
        $this->descripcioncorta = $descripcioncorta;

        return $this;
    }

    /**
     * Get descripcioncorta
     *
     * @return string
     */
    public function getDescripcioncorta()
    {
        return $this->descripcioncorta;
    }

    /**
     * Set caracteristicasuno
     *
     * @param string $caracteristicasuno
     * @return Productos
     */
    public function setCaracteristicasuno($caracteristicasuno)
    {
        $this->caracteristicasuno = $caracteristicasuno;

        return $this;
    }

    /**
     * Get caracteristicasuno
     *
     * @return string
     */
    public function getCaracteristicasuno()
    {
        return $this->caracteristicasuno;
    }

    /**
     * Set caracteristicasdos
     *
     * @param string $caracteristicasdos
     * @return Productos
     */
    public function setCaracteristicasdos($caracteristicasdos)
    {
        $this->caracteristicasdos = $caracteristicasdos;

        return $this;
    }

    /**
     * Get caracteristicasdos
     *
     * @return string
     */
    public function getCaracteristicasdos()
    {
        return $this->caracteristicasdos;
    }

    /**
     * Set caracteristicastres
     *
     * @param string $caracteristicastres
     * @return Productos
     */
    public function setCaracteristicastres($caracteristicastres)
    {
        $this->caracteristicastres = $caracteristicastres;

        return $this;
    }

    /**
     * Get caracteristicastres
     *
     * @return string
     */
    public function getCaracteristicastres()
    {
        return $this->caracteristicastres;
    }

    /**
     * Set caracteristicascuatro
     *
     * @param string $caracteristicascuatro
     * @return Productos
     */
    public function setCaracteristicascuatro($caracteristicascuatro)
    {
        $this->caracteristicascuatro = $caracteristicascuatro;

        return $this;
    }

    /**
     * Get caracteristicascuatro
     *
     * @return string
     */
    public function getCaracteristicascuatro()
    {
        return $this->caracteristicascuatro;
    }

    /**
     * Set caracteristicascinco
     *
     * @param string $caracteristicascinco
     * @return Productos
     */
    public function setCaracteristicascinco($caracteristicascinco)
    {
        $this->caracteristicascinco = $caracteristicascinco;

        return $this;
    }

    /**
     * Get caracteristicascinco
     *
     * @return string
     */
    public function getCaracteristicascinco()
    {
        return $this->caracteristicascinco;
    }

    /**
     * Set caracteristicasseis
     *
     * @param string $caracteristicasseis
     * @return Productos
     */
    public function setCaracteristicasseis($caracteristicasseis)
    {
        $this->caracteristicasseis = $caracteristicasseis;

        return $this;
    }

    /**
     * Get caracteristicasseis
     *
     * @return string
     */
    public function getCaracteristicasseis()
    {
        return $this->caracteristicasseis;
    }

    /**
     * Set stock
     *
     * @param string $stock
     * @return Productos
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Productos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Productos
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Add fotosproductos
     *
     * @param \Mediadiv\AdminBundle\Entity\FotosProductos $fotosproductos
     * @return Productos
     */
    public function addFotosproducto(\Mediadiv\AdminBundle\Entity\FotosProductos $fotosproductos)
    {
        $this->fotosproductos[] = $fotosproductos;

        return $this;
    }

    /**
     * Remove fotosproductos
     *
     * @param \Mediadiv\AdminBundle\Entity\FotosProductos $fotosproductos
     */
    public function removeFotosproducto(\Mediadiv\AdminBundle\Entity\FotosProductos $fotosproductos)
    {
        $this->fotosproductos->removeElement($fotosproductos);
    }

    /**
     * Get fotosproductos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFotosproductos()
    {
        return $this->fotosproductos;
    }

    /**
     * Set subcategorias
     *
     * @param \Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias
     * @return Productos
     */
    public function setSubcategorias(\Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias = null)
    {
        $this->subcategorias = $subcategorias;

        return $this;
    }

    /**
     * Get subcategorias
     *
     * @return \Mediadiv\AdminBundle\Entity\SubCategorias
     */
    public function getSubcategorias()
    {
        return $this->subcategorias;
    }
}
