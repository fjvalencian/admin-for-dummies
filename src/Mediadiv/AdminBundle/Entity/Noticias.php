<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Noticias
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\NoticiasRepository")
 */
class Noticias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255 , nullable=true)
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="contenido", type="text")
     */
    private $contenido;
    

    /** @ORM\Column(type="date") */
    
    
    
    private $fecha;



    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="noticias" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */

    

    private $tiposeccion;

    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     * @return Noticias
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Noticias
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set contenido
     *
     * @param string $contenido
     * @return Noticias
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;

        return $this;
    }

    /**
     * Get contenido
     *
     * @return string 
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Noticias
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return Noticias
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }
}
