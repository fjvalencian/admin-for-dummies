<?php
namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Asociados
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Asociados
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="foto", type="string", length=255 ,nullable=true )
     */
    private $foto;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;

     

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoAsociados", inversedBy="asociados" )
     * @ORM\JoinColumn(name="tipoasociados_id", referencedColumnName="id")
     */


    private $tipoasociados;




    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set foto
     *
     * @param string $foto
     * @return Asociados
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;

        return $this;
    }

    /**
     * Get foto
     *
     * @return string 
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Asociados
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     * @return Asociados
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set tipoasociados
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados
     * @return Asociados
     */
    public function setTipoasociados(\Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados = null)
    {
        $this->tipoasociados = $tipoasociados;

        return $this;
    }

    /**
     * Get tipoasociados
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoAsociados 
     */
    public function getTipoasociados()
    {
        return $this->tipoasociados;
    }
}
