<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorias
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Categorias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     * @ORM\Column(name="nombre", type="string", length=255 , nullable=true)
     */

    private $nombre;


    /**
     * @var string
     * @ORM\Column(name="descripcion", type="string", length=255 , nullable=true)
     */


    private $descripcion;



    /**
     * @var date
     * @ORM\Column(name="fechaIngreso", type="date",nullable=true)
     */




    private $fechaIngreso;




    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\SubCategorias", mappedBy="categorias" , cascade={"remove"})
     */

    private $subcategorias;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\FotoCategoria", mappedBy="categorias" , cascade={"remove"})
     */
    private $fotocategoria;


    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subcategorias = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fotocategoria = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Categorias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Categorias
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Categorias
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Add subcategorias
     *
     * @param \Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias
     * @return Categorias
     */
    public function addSubcategoria(\Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias)
    {
        $this->subcategorias[] = $subcategorias;

        return $this;
    }

    /**
     * Remove subcategorias
     *
     * @param \Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias
     */
    public function removeSubcategoria(\Mediadiv\AdminBundle\Entity\SubCategorias $subcategorias)
    {
        $this->subcategorias->removeElement($subcategorias);
    }

    /**
     * Get subcategorias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubcategorias()
    {
        return $this->subcategorias;
    }

    /**
     * Add fotocategoria
     *
     * @param \Mediadiv\AdminBundle\Entity\FotoCategoria $fotocategoria
     * @return Categorias
     */
    public function addFotocategorium(\Mediadiv\AdminBundle\Entity\FotoCategoria $fotocategoria)
    {
        $this->fotocategoria[] = $fotocategoria;

        return $this;
    }

    /**
     * Remove fotocategoria
     *
     * @param \Mediadiv\AdminBundle\Entity\FotoCategoria $fotocategoria
     */
    public function removeFotocategorium(\Mediadiv\AdminBundle\Entity\FotoCategoria $fotocategoria)
    {
        $this->fotocategoria->removeElement($fotocategoria);
    }

    /**
     * Get fotocategoria
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotocategoria()
    {
        return $this->fotocategoria;
    }
}
