<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TipoCliente
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class TipoCliente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="text")
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $descripcion;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\TipoSeccion", inversedBy="tipocliente" )
     * @ORM\JoinColumn(name="tiposeccion_id", referencedColumnName="id")
     */

    

    private $tiposeccion;



     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\AdminBundle\Entity\Posicion", inversedBy="tipocliente" )
     * @ORM\JoinColumn(name="posicion_id", referencedColumnName="id")
     */

        
    private $posicion;  


    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Clientes", mappedBy="tipocliente" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $clientes;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clientes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return TipoCliente
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return TipoCliente
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tiposeccion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion
     * @return TipoCliente
     */
    public function setTiposeccion(\Mediadiv\AdminBundle\Entity\TipoSeccion $tiposeccion = null)
    {
        $this->tiposeccion = $tiposeccion;

        return $this;
    }

    /**
     * Get tiposeccion
     *
     * @return \Mediadiv\AdminBundle\Entity\TipoSeccion 
     */
    public function getTiposeccion()
    {
        return $this->tiposeccion;
    }

    /**
     * Set posicion
     *
     * @param \Mediadiv\AdminBundle\Entity\Posicion $posicion
     * @return TipoCliente
     */
    public function setPosicion(\Mediadiv\AdminBundle\Entity\Posicion $posicion = null)
    {
        $this->posicion = $posicion;

        return $this;
    }

    /**
     * Get posicion
     *
     * @return \Mediadiv\AdminBundle\Entity\Posicion 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

    /**
     * Add clientes
     *
     * @param \Mediadiv\AdminBundle\Entity\Clientes $clientes
     * @return TipoCliente
     */
    public function addCliente(\Mediadiv\AdminBundle\Entity\Clientes $clientes)
    {
        $this->clientes[] = $clientes;

        return $this;
    }

    /**
     * Remove clientes
     *
     * @param \Mediadiv\AdminBundle\Entity\Clientes $clientes
     */
    public function removeCliente(\Mediadiv\AdminBundle\Entity\Clientes $clientes)
    {
        $this->clientes->removeElement($clientes);
    }

    /**
     * Get clientes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getClientes()
    {
        return $this->clientes;
    }
}
