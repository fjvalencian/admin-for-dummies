<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Posicion
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\AdminBundle\Entity\PosicionRepository")
 */
class Posicion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


 /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255 , nullable=true)
     */

    private $nombre;


      /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoAsociados", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $tipoasociados;


      /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoEmpresa", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $tipoempresa;



      /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoCliente", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */


    private $tipocliente;
    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoAccordion", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $tipoaccordion;



     /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\TipoEstadisticas", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $tipoestadisticas;
    
    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\AdminBundle\Entity\Slider", mappedBy="posicion" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $slider;


 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tipoasociados = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoempresa = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipocliente = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoaccordion = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tipoestadisticas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->slider = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Posicion
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add tipoasociados
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados
     * @return Posicion
     */
    public function addTipoasociado(\Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados)
    {
        $this->tipoasociados[] = $tipoasociados;

        return $this;
    }

    /**
     * Remove tipoasociados
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados
     */
    public function removeTipoasociado(\Mediadiv\AdminBundle\Entity\TipoAsociados $tipoasociados)
    {
        $this->tipoasociados->removeElement($tipoasociados);
    }

    /**
     * Get tipoasociados
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoasociados()
    {
        return $this->tipoasociados;
    }

    /**
     * Add tipoempresa
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa
     * @return Posicion
     */
    public function addTipoempresa(\Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa)
    {
        $this->tipoempresa[] = $tipoempresa;

        return $this;
    }

    /**
     * Remove tipoempresa
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa
     */
    public function removeTipoempresa(\Mediadiv\AdminBundle\Entity\TipoEmpresa $tipoempresa)
    {
        $this->tipoempresa->removeElement($tipoempresa);
    }

    /**
     * Get tipoempresa
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoempresa()
    {
        return $this->tipoempresa;
    }

    /**
     * Add tipocliente
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente
     * @return Posicion
     */
    public function addTipocliente(\Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente)
    {
        $this->tipocliente[] = $tipocliente;

        return $this;
    }

    /**
     * Remove tipocliente
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente
     */
    public function removeTipocliente(\Mediadiv\AdminBundle\Entity\TipoCliente $tipocliente)
    {
        $this->tipocliente->removeElement($tipocliente);
    }

    /**
     * Get tipocliente
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipocliente()
    {
        return $this->tipocliente;
    }

    /**
     * Add tipoaccordion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion
     * @return Posicion
     */
    public function addTipoaccordion(\Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion)
    {
        $this->tipoaccordion[] = $tipoaccordion;

        return $this;
    }

    /**
     * Remove tipoaccordion
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion
     */
    public function removeTipoaccordion(\Mediadiv\AdminBundle\Entity\TipoAccordion $tipoaccordion)
    {
        $this->tipoaccordion->removeElement($tipoaccordion);
    }

    /**
     * Get tipoaccordion
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoaccordion()
    {
        return $this->tipoaccordion;
    }

    /**
     * Add tipoestadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas
     * @return Posicion
     */
    public function addTipoestadistica(\Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas)
    {
        $this->tipoestadisticas[] = $tipoestadisticas;

        return $this;
    }

    /**
     * Remove tipoestadisticas
     *
     * @param \Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas
     */
    public function removeTipoestadistica(\Mediadiv\AdminBundle\Entity\TipoEstadisticas $tipoestadisticas)
    {
        $this->tipoestadisticas->removeElement($tipoestadisticas);
    }

    /**
     * Get tipoestadisticas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTipoestadisticas()
    {
        return $this->tipoestadisticas;
    }

    /**
     * Add slider
     *
     * @param \Mediadiv\AdminBundle\Entity\Slider $slider
     * @return Posicion
     */
    public function addSlider(\Mediadiv\AdminBundle\Entity\Slider $slider)
    {
        $this->slider[] = $slider;

        return $this;
    }

    /**
     * Remove slider
     *
     * @param \Mediadiv\AdminBundle\Entity\Slider $slider
     */
    public function removeSlider(\Mediadiv\AdminBundle\Entity\Slider $slider)
    {
        $this->slider->removeElement($slider);
    }

    /**
     * Get slider
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSlider()
    {
        return $this->slider;
    }
}
