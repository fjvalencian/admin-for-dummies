<?php

namespace Mediadiv\AdminBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * RedesSociales
 *
 * @ORM\Table()
 * @ORM\Entity
 */

class RedesSociales {
	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="icono", type="text" , nullable=true)
	 */
	private $icono;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="link", type="text")
	 */
	private $link;
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="text")
	 */
	private $title;
        
        /**
	 * @var string
	 *
	 * @ORM\Column(name="target", type="text")
	 */
	private $target;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set icono
     *
     * @param string $icono
     * @return RedesSociales
     */
    public function setIcono($icono)
    {
        $this->icono = $icono;

        return $this;
    }

    /**
     * Get icono
     *
     * @return string 
     */
    public function getIcono()
    {
        return $this->icono;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return RedesSociales
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return RedesSociales
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set target
     *
     * @param string $target
     * @return RedesSociales
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return string 
     */
    public function getTarget()
    {
        return $this->target;
    }
}
