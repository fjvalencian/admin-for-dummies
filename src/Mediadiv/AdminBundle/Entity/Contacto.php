<?php

namespace Mediadiv\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contacto
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Contacto
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text")
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255 , nullable=true)
     */
    private $telefono;



        /**
     * @var string
     *
     * @ORM\Column(name="telefono2", type="string", length=255 , nullable=true)
     */
    private $telefono2;


    /**
     * @var string
     *
     * @ORM\Column(name="emailEnvio", type="string", length=255)
     */
    private $emailEnvio;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255)
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="lagitude", type="string", length=255)
     */
    private $lagitude;





    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contacto
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Contacto
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Contacto
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set telefono2
     *
     * @param string $telefono2
     * @return Contacto
     */
    public function setTelefono2($telefono2)
    {
        $this->telefono2 = $telefono2;

        return $this;
    }

    /**
     * Get telefono2
     *
     * @return string 
     */
    public function getTelefono2()
    {
        return $this->telefono2;
    }

    /**
     * Set emailEnvio
     *
     * @param string $emailEnvio
     * @return Contacto
     */
    public function setEmailEnvio($emailEnvio)
    {
        $this->emailEnvio = $emailEnvio;

        return $this;
    }

    /**
     * Get emailEnvio
     *
     * @return string 
     */
    public function getEmailEnvio()
    {
        return $this->emailEnvio;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Contacto
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set lagitude
     *
     * @param string $lagitude
     * @return Contacto
     */
    public function setLagitude($lagitude)
    {
        $this->lagitude = $lagitude;

        return $this;
    }

    /**
     * Get lagitude
     *
     * @return string 
     */
    public function getLagitude()
    {
        return $this->lagitude;
    }
}
