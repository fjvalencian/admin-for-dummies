<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AsociadosType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

        ->add('titulo','text', array(
                    'label' => 'Título',
                    'attr' => array(
                        'class' => 'form-control'
                    )))


         ->add('texto','textarea', array(
                    'label' => 'Texto : ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
               )))


 
            ->add('tipoasociados', 'entity', array(
                    'class' => 'MediadivAdminBundle:TipoAsociados',
                    'property' => 'titulo',
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Tipo sección',
                    'required' => false,
                    'empty_value' => 'Seleccione Asociados:',
                    'empty_data' => null,
                        )
        )

            ;

       
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Asociados'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_asociados';
    }
}
