<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactoType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder


 

            ->add('email' ,'text', array(
                    'label' => 'Email',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))



            ->add('direccion' ,'textarea', array(
                    'label' => 'Dirección',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                        
                    )



                ))



            ->add('telefono','text', array(
                    'label' => 'Teléfono 1',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


            ->add('telefono2','text', array(
                    'label' => 'Teléfono 2' ,
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


            ->add('emailEnvio' ,'text', array(
                    'label' => 'Email de envío',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                )) 
            ->add('latitude' ,'text', array(
                    'label' => 'Latitud',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))



            ->add('lagitude','text', array(
                    'label' => 'Longitud',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Contacto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_contacto';
    }
}
