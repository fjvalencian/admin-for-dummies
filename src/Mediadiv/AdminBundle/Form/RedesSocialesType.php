<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RedesSocialesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('link', 'text', array(
                    'label' => 'Link',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('title', 'text', array(
                    'label' => 'Título del vínculo',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('icono', 'text', array(
                    'label' => 'Ícono',
                    'attr' => array(
                        'class' => 'form-control icp icp-auto'
            )))
                ->add('target', 'choice', array(
                    'label' => 'Seleccione su Target:',
                    'choices' => array('_blank' => '_blank', '_parent' => '_parent'),
                    'attr' => array(
                        'class' => 'form-control icp icp-auto'
            )))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\RedesSociales'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_redessociales';
    }

}
