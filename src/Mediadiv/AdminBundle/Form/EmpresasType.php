<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EmpresasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                    'label' => 'Nombre',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )

                ))

            ->add('tipoempresa', 'entity', array(
                    'class' => 'MediadivAdminBundle:TipoEmpresa',
                    'property' => 'titulo',
                 
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Tipo sección',
                    'required' => false,
                    'empty_value' => 'Seleccione el tipo Clientes',
                    'empty_data' => null,
                        )
        )
            ;
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Empresas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_empresas';
    }
}
