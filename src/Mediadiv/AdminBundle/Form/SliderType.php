<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SliderType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('posicion', 'entity', array(
                    'class' => 'MediadivAdminBundle:Posicion',
                    'property' => 'nombre',
                    'attr' => array('class' => 'form-control'),
                    'label' => 'Posición ',
                    'required' => false,
                    'empty_value' => 'Opción',
                    'empty_data' => null,
                        )
                )
                ->add('titulo', 'text', array(
                    'label' => 'Título',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('bajadatexto', 'text', array(
                    'label' => 'Bajada de texto.',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('vermas', 'text', array(
                    'label' => 'Link (ver más):',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('target', 'choice', array(
                    'label' => 'Seleccione Target:',
                    'choices' => array(
                        '_blank' => '_blank',
                        '_parent' => '_parent'
                    ),
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('tiposeccion',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:TipoSeccion',
                       'property' => 'titulo',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Contenido en: ',
                       'required' => false,
                       'empty_value' => 'Seleccione la sección',
                       'empty_data' => null,
            ))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Slider'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_slider';
    }

}
