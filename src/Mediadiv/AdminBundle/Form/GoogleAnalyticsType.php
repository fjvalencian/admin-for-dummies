<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoogleAnalyticsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('script', 'text', array(
                    'required' => false,
                    'label' => ' Pegar acá el código Analytics que desea para realizar un seguimiento.',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('estado', 'choice', array(
                    'label' => 'Seleccione su estado',
                    'choices' => array('1' => 'Activado', '0' => 'Desactivado'),
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\GoogleAnalytics'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_googleanalytics';
    }

}
