<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TagsType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        ->add('descripcion', 'textarea', array(
        'label' => 'Ingrese Tag:',
        array(
        'attr' => array('class' => 'form-control')
        )
        ))
        ->add('estado', 'choice',
        array(
        'label' => 'Seleccione un estado: ',
        array(
        'attr' => array('class' => 'form-control')
        ),
            'choices' => array('1' => 'Activado', '0'=>'Desactivado')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Tags'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_tags';
    }

}
