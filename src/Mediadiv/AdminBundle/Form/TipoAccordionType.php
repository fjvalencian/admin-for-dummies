<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TipoAccordionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo', 'text', array(
                'label' => 'Título:',
                'attr' => array('class' => 'form-control')
                ))
            ->add('descripcion', 'textarea', array(
                'label' => 'Descripción de la sección:',
                'attr' => array('class' => 'form-control')
                ))
            ->add('posicion',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:Posicion',
                       'property' => 'nombre',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Posícion ',
                       'required' => false,
                       'empty_value' => 'Opcíon',
                       'empty_data' => null,
            ))

            ->add('tiposeccion',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:TipoSeccion',
                       'property' => 'titulo',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Contenido en: ',
                       'required' => false,
                       'empty_value' => 'Seleccione el tipo de accordion',
                       'empty_data' => null,
            ))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\TipoAccordion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_tipoaccordion';
    }
}
