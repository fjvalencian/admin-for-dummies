<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        
            


                ->add('nombre', 'text', array(
                    'label' => 'Nombre',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control'
                    )

                ))


                 ->add('profesion', 'text', array(
                    'label' => 'Profesión',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control'
                    )

                ))


                   ->add('texto', 'textarea', array(
                    'label' => 'Texto ',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )

                ))

                ->add('tipocliente',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:TipoCliente',
                       'property' => 'titulo',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Contenido en Testimonio',
                       'required' => true,
                       'empty_value' => 'Contenido en Testimonio',
                       'empty_data' => null,
                   )
                )
                ;   




    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Clientes'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_clientes';
    }
}
