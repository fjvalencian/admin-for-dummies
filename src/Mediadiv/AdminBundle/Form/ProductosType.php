<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

//Prueba, agregando listener al formulario.
//use Symfony\Component\Form\FormEvent;
//use Symfony\Component\Form\FormEvents;

class ProductosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('codigo')
            ->add('descripcion')
            ->add('descripcioncorta')
            ->add('caracteristicasuno')
           ->add('caracteristicasdos')
            ->add('stock')
            ->add('estado')
                
//            ->add('categorias',
//                  'entity',
//                  array(
//                        'class' => 'MediadivAdminBundle:Categorias',
//                        'property' => 'nombre',
//                        'attr' => array('class' => 'form-control'),
//                        'label' => 'Categoria',
//                        'required' => false,
//                        'empty_value' => 'Selecciona una categoria',
//                        'empty_data' => null
//                  )  
//                 )
                
            ->add('subcategorias',
                  'entity',
                  array(
                      'class' => 'MediadivAdminBundle:SubCategorias', 
                      'property' => 'nombre',
                      'attr' => array('class' => 'form-control'),
                      'label' => 'SubCategoria',
                      'required' => false,
                      'empty_value' => 'Seleccione Una subcategoria',
                      'empty_data' => null
                  )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Productos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_productos';
    }
}
