<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TitlesType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
        ->add('title', 'text', array(
        'label' => 'Ingrese el título de su página:',
        'attr' => array('class' => 'form-control')
        ))

        ->add('estado', 'choice',
            array(
            'choices' => array('1' => 'Activado', '0' => 'Desactivado'),
            'label' => 'Seleccione un estado:',
            'attr' => array('class' => 'form-control')
            )
        )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Titles'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_titles';
    }

}
