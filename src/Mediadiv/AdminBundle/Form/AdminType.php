<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text',array(
                    'attr' => array('class' => 'form-control')
                    ))
            ->add('password', 'password',
                    array(
                        'attr' => array('class' => 'form-control')
                    )
                    )
            ->add('email', 'email', array(
                'attr' => array('class' => 'form-control')
            ))
            ->add('isActive', 'choice', array(
                     'choices' => array(
                        '1' => 'Activar',
                        '0' => 'Desactivar'
                    ))
                    )
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Admin'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_admin';
    }
}
