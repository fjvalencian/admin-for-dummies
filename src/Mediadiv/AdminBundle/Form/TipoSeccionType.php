<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TipoSeccionType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('tipoplantilla', 'choice', array('label_attr' => array('class' => 'oculto'),
                    'choices' => array('plantilla1' => 'Plantilla 1', 'plantilla2' => 'Plantilla 2')
                    , 'expanded' => true, 'multiple' => false, 'attr' => array(
                        'class' => 'oculto'
                    )
                ))
                ->add('titulo', 'text', array(
                    'label' => 'Ingrese Título:',
                    'attr' => array(
                        'class' => 'form-control'
            )))

                
            
                ->add('descripcion', 'text', array(
                    'label' => 'Descripción ',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\TipoSeccion'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_tiposeccion';
    }

}
