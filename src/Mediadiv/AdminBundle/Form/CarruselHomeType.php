<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CarruselHomeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','text', array(
                    'label' => 'Título',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


            ->add('contenido', 'textarea', array(
                    'label' => 'Contenido',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    )

                ))

            ->add('tipoaccordion',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:TipoAccordion',
                       'property' => 'titulo',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Contenido en: ',
                       'required' => false,
                       'empty_value' => 'Seleccione el tipo de accordion',
                       'empty_data' => null,
                   )
                );



            

            
        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\CarruselHome'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_carruselhome';
    }
}
