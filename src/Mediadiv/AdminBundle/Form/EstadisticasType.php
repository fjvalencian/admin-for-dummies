<?php

namespace Mediadiv\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EstadisticasType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre','text', array(
                    'label' => 'Nombre',

                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


            ->add('cantidad','integer', array(
                    'label' => 'Cantidad',

                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


             ->add('icon','text', array(
                    'label' => 'Ícono',

                    'attr' => array(
                        'class' => 'form-control icp icp-auto'
                    )



                ))


             
  
              ->add('tipoestadisticas',
                  'entity',
                   array(
                       'class' => 'MediadivAdminBundle:TipoEstadisticas',
                       'property' => 'titulo',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Seleccione su sección ',
                       'required' => false,
                       'empty_value' => 'Seleccione su sección',
                       'empty_data' => null,
                   )
                );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\AdminBundle\Entity\Estadisticas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_estadisticas';
    }
}
