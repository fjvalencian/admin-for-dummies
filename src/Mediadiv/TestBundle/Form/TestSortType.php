<?php

namespace Mediadiv\TestBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TestSortType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('posicion', 'text', array(
                'label' => 'Ingresa la posición: ',
                'attr' => array(
                    'class' => 'form-control'
                )))
            ->add('nombre', 'text', array(
                'label' => 'Ingresa el nombre: ',
                'attr' => array(
                    'class' => 'form-control'
                )))
            ->add('descripcion', 'textarea', array(
                'label' => 'Ingresa una descripción:',
                'attr' => array(
                    'class' => 'form-control'
                )))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\TestBundle\Entity\TestSort'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_testbundle_testsort';
    }
}
