CREATE DATABASE  IF NOT EXISTS `dimaco` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_estonian_ci */;
USE `dimaco`;
-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: dimaco
-- ------------------------------------------------------
-- Server version	5.5.37-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Admin`
--

DROP TABLE IF EXISTS `Admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Admin`
--

LOCK TABLES `Admin` WRITE;
/*!40000 ALTER TABLE `Admin` DISABLE KEYS */;
INSERT INTO `Admin` VALUES (1,'admin','admin');
/*!40000 ALTER TABLE `Admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Categorias`
--

DROP TABLE IF EXISTS `Categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categorias`
--

LOCK TABLES `Categorias` WRITE;
/*!40000 ALTER TABLE `Categorias` DISABLE KEYS */;
INSERT INTO `Categorias` VALUES (1,'Arriendo de Herramientas','Arriendo de Herramientas','2014-06-24'),(2,'Arriendo de Maquinarias','Arriendo de Maquinarias','2014-06-24');
/*!40000 ALTER TABLE `Categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FotoCategoria`
--

DROP TABLE IF EXISTS `FotoCategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FotoCategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `categorias_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C2501215792B277` (`categorias_id`),
  CONSTRAINT `FK_C2501215792B277` FOREIGN KEY (`categorias_id`) REFERENCES `Categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FotoCategoria`
--

LOCK TABLES `FotoCategoria` WRITE;
/*!40000 ALTER TABLE `FotoCategoria` DISABLE KEYS */;
INSERT INTO `FotoCategoria` VALUES (1,1,'icon_tools.jpg'),(2,2,'icon_machinery.jpg');
/*!40000 ALTER TABLE `FotoCategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FotoSlider`
--

DROP TABLE IF EXISTS `FotoSlider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FotoSlider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DFB946102CCC9638` (`slider_id`),
  CONSTRAINT `FK_DFB946102CCC9638` FOREIGN KEY (`slider_id`) REFERENCES `Slider` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FotoSlider`
--

LOCK TABLES `FotoSlider` WRITE;
/*!40000 ALTER TABLE `FotoSlider` DISABLE KEYS */;
INSERT INTO `FotoSlider` VALUES (1,1,'2014-06-24 18:29:56','slider_slide_1.jpg'),(2,2,'2014-06-24 18:30:31','slider_slide_2.jpg'),(3,3,'2014-06-24 18:30:48','slider_slide_3.jpg'),(4,4,'2014-06-24 18:31:08','slider_slide_4.jpg');
/*!40000 ALTER TABLE `FotoSlider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FotosProductos`
--

DROP TABLE IF EXISTS `FotosProductos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FotosProductos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productos_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `estado` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AAD12AEEED07566B` (`productos_id`),
  CONSTRAINT `FK_AAD12AEEED07566B` FOREIGN KEY (`productos_id`) REFERENCES `Productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FotosProductos`
--

LOCK TABLES `FotosProductos` WRITE;
/*!40000 ALTER TABLE `FotosProductos` DISABLE KEYS */;
INSERT INTO `FotosProductos` VALUES (1,1,'2014-06-24 18:22:03',NULL,'kit_barreno.jpg'),(2,2,'2014-06-24 18:22:36',NULL,'kit_horquilla_500kg.jpg'),(3,3,'2014-06-24 18:24:35',NULL,'kit_martillo_516j.jpg'),(4,4,'2014-06-24 18:25:11',NULL,'minicargador_robot170.jpg'),(5,5,'2014-06-24 18:26:22',NULL,'cincelador_makita480mm.jpg'),(6,6,'2014-06-24 18:27:05',NULL,'cincelador_dw.jpg'),(7,7,'2014-06-24 18:27:36',NULL,'martillo_rompedor_bosch.jpg'),(8,8,'2014-06-24 18:28:16',NULL,'martillo_demoledor_bosch.jpg');
/*!40000 ALTER TABLE `FotosProductos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Productos`
--

DROP TABLE IF EXISTS `Productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcategorias_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcioncorta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicasuno` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicasdos` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicastres` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicascuatro` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicascinco` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicasseis` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `estado` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F48512451A9B97E8` (`subcategorias_id`),
  CONSTRAINT `FK_F48512451A9B97E8` FOREIGN KEY (`subcategorias_id`) REFERENCES `SubCategorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Productos`
--

LOCK TABLES `Productos` WRITE;
/*!40000 ALTER TABLE `Productos` DISABLE KEYS */;
INSERT INTO `Productos` VALUES (1,7,'Kit Barreno 1200 mm P/R-170','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(2,7,'Kit Horquilla 500 KG P/R-170','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(3,7,'Kit Martillo 516J P/R-170','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(4,7,'Minicargador','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(5,1,'Cincelador 480 mm 8,7 Kg','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(6,1,'Cincelador 10,5 kg','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(7,1,'Martillo Rompedor 5,5 Kg','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24'),(8,1,'Martillo Demoledor 29 Kg','HM1201','En construcción y obra mediana, demolición de losas, muros y radieres.','8,7 kg','Makita','','',NULL,NULL,NULL,NULL,'activado','2014-06-24');
/*!40000 ALTER TABLE `Productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Slider`
--

DROP TABLE IF EXISTS `Slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `texto2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Slider`
--

LOCK TABLES `Slider` WRITE;
/*!40000 ALTER TABLE `Slider` DISABLE KEYS */;
INSERT INTO `Slider` VALUES (1,'Producto 01','Pequeña reseña del producto 01'),(2,'Producto 02','Pequeña reseña del producto 02'),(3,'Producto 03','Pequeña reseña del producto 03'),(4,'Producto 04','Pequeña reseña del producto 04');
/*!40000 ALTER TABLE `Slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SubCategorias`
--

DROP TABLE IF EXISTS `SubCategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SubCategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `categorias_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD2D2CEA5792B277` (`categorias_id`),
  CONSTRAINT `FK_FD2D2CEA5792B277` FOREIGN KEY (`categorias_id`) REFERENCES `Categorias` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SubCategorias`
--

LOCK TABLES `SubCategorias` WRITE;
/*!40000 ALTER TABLE `SubCategorias` DISABLE KEYS */;
INSERT INTO `SubCategorias` VALUES (1,'Demolición','2014-06-24',1),(2,'Perforación','2014-06-24',1),(3,'Carpintería','2014-06-24',1),(4,'Compactación','2014-06-24',1),(5,'Fijación','2014-06-24',1),(6,'Soldaduras','2014-06-24',1),(7,'Movimiento de Tierra','2014-06-24',2),(8,'Maquinas para soldar','2014-06-24',2);
/*!40000 ALTER TABLE `SubCategorias` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-24 18:33:36
